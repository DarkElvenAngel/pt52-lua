#ifndef CHARBUFFER_IMPROVED_H
#define CHARBUFFER_IMPROVED_H

#include <stdio.h>
#include "pico.h"
#include "pico/stdlib.h"
#include "attributes.h"
#include <stdarg.h>
#include "font.h"
#include "config.h"

// OVERRIDE STDOUT FUNCTIONS WITH INTERNAL VERSIONS
#undef printf
#define printf Printf
#undef putchar
#define putchar Decode_Character
#undef puts
#define puts Puts

/* Replace these with Variables so that we can work
with variable sized screens. 
#define MAX_COL CHARBUFFER_COLUMNS
#define MAX_LINE CHARBUFFER_LINES */
extern uint8_t MAX_COL;
extern uint8_t MAX_LINE;
#define DEFAULT_COLOURS CHARBUFFER_DEFAULT_COLOURS
#define DEFAULT_PALLET CHARBUFFER_DEFAULT_PALLET

#define char_buffer _VBUFFERS.TEXT_BUFF

#pragma pack(1)
typedef struct{
    union {
        struct {
            uint8_t ctrl : 1;
            uint8_t shift : 1;
            uint8_t alt : 1;
            uint8_t wkey : 1;
        };
        uint8_t Flags;
    };
    char character;
    uint8_t keycode;
} keycode_t;

#pragma pack(1)

struct CPostion_s
{
    int8_t Column;
    int8_t Line;
    union
    {
        struct {
            uint8_t underline   : 1;
            uint8_t reverse     : 1;
            uint8_t extended    : 1;
#ifdef ENABLE_VT100
            uint8_t intensity   : 1;
#endif
#if 0   // This is not efficient
            uint8_t bold        : 1;
            uint8_t codepage437 : 1;
            uint8_t fontBuffer  : 1;
#endif
        /* Select what font to render with.
         * 0 - Normal
         * 1 - BOLD
         * 2 - Code Page 437
         * 3 - Font Buffer
         */
            uint8_t Use_Font    : 2; 
        };
        uint8_t Flags;
    };
    union
    {
        struct {
            uint8_t foreground_colour;
            uint8_t background_colour;
        };
        uint16_t colours;
    };
    int8_t Pallet;
};
struct CB_Flags_s
{
    uint8_t Line_Wrap           : 1;
    uint8_t Cr_Lf               : 1;
    uint8_t Origin_mode         : 1;
    uint8_t PT52_mode           : 1;
    uint8_t Top_Margin;
    uint8_t Bottom_Margin;

};


//extern BUFFER_TYPE char_buffer[MAX_LINE][MAX_COL];
#pragma pack(1)
typedef struct _Sprite
{
    uint64_t h:8;               // Height
    uint64_t w:8;               // Width
    int64_t  x:11;              // Postion x
    int64_t  y:9;               // Postion y
    uint64_t colour_depth:2;    // Colour Depth
    uint64_t data_offset:13;    // Offset in sprite buffer
    uint64_t data_length:6;     // Length of sprite data
    uint64_t pallet_shift:4;    // Length of sprite data
    uint64_t scale_x:1;         // Scale x
    uint64_t scale_y:1;         // Scale y
    uint64_t use_all_colours:1; // No Transparency
} _SPRITE_HEADER_T;
#define SPRITE_SIZE sizeof(uint64_t) //struct _Sprite)
#pragma pack(1)
struct _VRAM_REG
{
    uint8_t _Display_Mode:3;            // 0
    uint8_t _Display_Flip:1;

    struct CPostion_s Cursor;
    struct CPostion_s Cursor_Store;
    struct CB_Flags_s Buffer_Flags;
    uint8_t MAX_LINE;                   // 16
    uint8_t MAX_COL;                    // 17
    // BOOLEANS
    uint8_t Cursor_Blink:1;
    uint8_t _Blink:1;
    uint8_t led_state:1;
    uint8_t KeyBoard_Mounted:1;
    uint8_t LAMPS:4;                    // 18
    uint8_t keylock_flags;              // 19

    char last_char;                     // 20
    keycode_t last_keycode;             // 21 - 23
    union {
        struct {
            uint8_t _KEYBOARD_FN_KEYS:1;
            uint8_t _KEYBOARD_CTRL_C:1;
            uint8_t _KEYBOARD_LOCAL_ECHO:1;
        };
        uint8_t _KEYBOARD_FLAGS;
    };                                  // 24
    uint8_t _KEYBOARD_MOD_KEYS;         // 25
    uint8_t _KEYBOARD_KEY_CODES[6];     // 26 - 31

    uint8_t HEADER_FG:4;                // 32
    uint8_t HEADER_BG:4;
    char    HEADER[80];                 // 112
    uint8_t FOOTER_FG:4;
    uint8_t FOOTER_BG:4;                // 113
    char    FOOTER[80];                 // 193
    char    STATUS[80];                 // 273

    uint8_t HEAD_ENABLE:1;
    uint8_t HEAD_FONT:2;
    uint8_t FOOT_ENABLE:1;
    uint8_t FOOT_FONT:2;
    uint8_t STAT_ENABLE:1;
    uint8_t STAT_USER:1;                // 274
    uint8_t HEAD_Pallet:3;
    uint8_t FOOT_Pallet:3;
    uint8_t HSR_RES    :2;              // 275

    uint8_t OVERLAY_X : 8;              // 276
    uint8_t OVERLAY_Y : 8;              // 277
    uint8_t OVERLAY_W : 8;              // 278
    uint8_t OVERLAY_H : 8;              // 279
    uint8_t OVERLAY_CLUT : 2;           // 280
    uint8_t OVERLAY_X_SCALE     : 1; 
    uint8_t OVERLAY_Y_SCALE     : 1; 
    uint8_t OVERLAY_ENABLE      : 1; 
    uint8_t OVERLAY_TRANSPARENT : 1; 
    uint8_t OVERLAY_PROTECT     : 1; 
    uint8_t OVERLAY_RESERVED    : 1;
    uint8_t OVERLAY_CLUT_WIN;           // 281
};
#define VRAM_REG_SIZE sizeof(struct _VRAM_REG) // LIMIT 384

#pragma pack(1)
struct _UVB {
    union {
      struct {
        union {
            BUFFER_TYPE TEXT_BUFF[CHARBUFFER_LINES][CHARBUFFER_COLUMNS];
            struct {
                uint8_t             VIDEO_BUFF[16000];
                union{
                    struct _VRAM_REG    VIDEO_REG;
                    uint8_t             VIDEO_REG_[384];
                };
            };
        };
        uint16_t            CLUT[256];
        uint16_t            SPRITE_CLUT[16];
        union{
            _SPRITE_HEADER_T    SPRITE_HEADERS[16];
            uint8_t             SPRITE_REGS[128];
        };
        uint8_t             SPRITE_BUFF[4096];
        uint8_t             FONT_BUFFER[2048];
        uint8_t             XFONT_BUFFER[352];
        uint8_t             OVRLY_BUFFER[8000];
        uint16_t            OVRLY_CLUT[16];
      };
      uint8_t               BUFFER_RAW[31584]; // 23552];
    };
};

#define BUFFERSIZE  sizeof(struct _UVB);
extern struct _UVB _VBUFFERS;

#define Cursor _VBUFFERS.VIDEO_REG.Cursor
#define Cursor_Store _VBUFFERS.VIDEO_REG.Cursor_Store
#define Buffer_Flags _VBUFFERS.VIDEO_REG.Buffer_Flags

#define MAX_LINE _VBUFFERS.VIDEO_REG.MAX_LINE
#define MAX_COL  _VBUFFERS.VIDEO_REG.MAX_COL

#define KeyBoard_Mounted _VBUFFERS.VIDEO_REG.KeyBoard_Mounted
#define keylock_flags _VBUFFERS.VIDEO_REG.keylock_flags
#define led_state _VBUFFERS.VIDEO_REG.led_state

#define last_char _VBUFFERS.VIDEO_REG.last_char
#define last_keycode _VBUFFERS.VIDEO_REG.last_keycode

#define DECOM   Buffer_Flags.Origin_mode
#define DECAWM  Buffer_Flags.Line_Wrap
#define LNM     Buffer_Flags.Cr_Lf
#define DECANM  Buffer_Flags.PT52_mode
#define SET     1
#define RESET   0

/**
 * Reset Buffer 
 * 
 * \return none
 */
void Reset_Character_Buffer();

/**
 * Scroll Buffer and create new line.
 * 
 * 0 Down bottom line is truncated, new line added at top
 * 1 Up top line is truncated, newline added at bottom.
 * 
 * \param direction of scroll
 * \return none
 */
void Scroll(bool direction);
/**
 * Place cursor at a specific location
 * 
 * \param column Column number
 * \param line Line number
 * \return none
 */
void Move_cursorto(uint8_t column, uint8_t line);
/**
 * Move the cursor in on direction
 * 
 * \param plane 0 up/down : 1 left/right 
 * \param n number or positions
 * \return none
 */
void Move_cursor(bool plane, int16_t n);
/**
 * Place the cursor at the home position
 * 
 * \param mode 0 Origin : 1 Line
 * \return none
 */
/**
 * Save cursor location and if enabled attributes and colour
 * 
 * \return none
 */
void Save_cursor();
/**
 * Restore cursor location and if enabled attributes and colour
 * 
 * \return none
 */
void Restore_cursor();

void Home(bool mode);
/**
 * Clear Screen
 * 
 * 0 from cursor to end of buffer
 * 1 from begining of buffer to cursor
 * 2 entire buffer
 * 
 * \param option select operation
 * \return none
 */
void Clear_screen(uint8_t option);
/**
 * Clear Current Line
 * 
 * 0 from cursor to end of line
 * 1 from begining of line to cursor
 * 2 entire line
 * 
 * \param option select operation
 * \return none
 */
void Clear_line(uint8_t option);
/**
 * Set scrolling region
 * This is use to only scroll part of the display
 * 
 * \param top line number where region starts.  use 0 to reset
 * \param bottom line number where region ends. use 0 for reset 
 * \return none
 */
void Set_scrollregion(uint8_t top, uint8_t bottom);
/**
 * Set origin mode
 * 
 * \param value SET - Absolute RESET - Relative
 * \return none
 */
void Set_originmode(bool bit);
/// writing to the buffer

/**
 * put a character at a location.
 * This function does not effect cursor location.
 * 
 * \param column Column number
 * \param line Line number
 * \param character what to place
 * \return none
 */
void Put_charp(uint8_t column, uint8_t line, const char character);
/**
 * put a character at the current position.
 * This function does effect cursor location!
 * 
 * \param character what to place
 * \return none
 */
void Put_char(const char character);
/**
 * Write formated output to the buffer and update position of cursor
 * 
 * \param character what to place
 * \return number  of characters printed
 */
int Printf (const char *format, ...);
/**
 * Write a string, followed by a newline, to screen.
 * 
 * \param const char 
 * \return none
 */
void Puts(const char *s);
/**
 * Write formatted output to buffer at a location.
 * This function does not effect cursor location.
 * 
 * \param column Column number
 * \param line Line number
 * \param Format controls the output as in C printf.
 * \return none
 */
void Printfp (uint8_t column, uint8_t line, const char *format, ... );

//
/**
 * CarriageReturn
 * 
 * \return none
 */
void CarriageReturn();
/**
 * Linefeed
 *  
 * \return none
 */
void Linefeed(); 
/* Reverse_Linefeed
 *  
 * \return none
 */
void Reverse_Linefeed();
/**
 * Backspace
 * 
 * \return none
 */
void Backspace();
/**
 * Tab
 * 
 * \return none
 */
void Tab();
/**
 * Enter
 * 
 * \return none
 */
void Enter();
/**
 * Cursor Position Check verify Cursor is within buffer bounds.
 * 
 * \return none
 */
void Cursor_Position_Check();
/**
 * Decode_Character
 * 
 * \param character what character to decode.
 * \return none
 */
void Decode_Character(char character);

#endif