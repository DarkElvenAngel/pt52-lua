/*#include <unistd.h>
#include <string.h>
#include <pthread.h>*/
// Only for the debug printf 
#if 0
#ifndef Q3_VM 
#include <limits.h>
#include <stdio.h> 
#include <string.h>
#define HEAP_SIZE 1024
#else
#include "pt52io.h"
#include "pt52_mem.h"
#endif
#else
#include "pt52io.h"
#include <string.h>
# ifndef HEAP_SIZE
#define HEAP_SIZE 1024 * 175
# endif
#endif

#ifndef NULL
#define NULL ((void *)0)
#endif

// typedef long intptr_t;
char HEAP[HEAP_SIZE] = { 0 }; 

#if 0
typedef char ALIGN[32];


union header {
	struct {
		unsigned int size;
		unsigned int is_free;
		union header *next;
	} s;
	/* force the header to be aligned to 16 bytes */
	ALIGN stub;
};
typedef union header header_t;
#define HEADER_SIZE sizeof(header_t)

header_t *head = NULL, *tail = NULL;

int align(int n) {
  return (n + sizeof(double) - 1) & ~(sizeof(double) - 1);
}

static char* pt_mem_index = HEAP;
static uint32_t pt_mem_used = 0;

static void *sbrk(int32_t increment)
{
	char* start = pt_mem_index;
	if (increment == 0) return (void*)pt_mem_index;
	if (increment > 0)
	{
		if ((char*)(pt_mem_index + increment) > &HEAP[HEAP_SIZE]) return (void*) -1;
	} else {
		if ((char*)(pt_mem_index + increment) < &HEAP[0]) return (void*) -1;
	}
	pt_mem_index += increment;
	pt_mem_used += increment;
	return (void *)start;
}

header_t *get_free_block(size_t size)
{
	header_t *curr = head;
	while(curr) {
		/* see if there's a free block that can accommodate requested size */
		if (curr->s.is_free && curr->s.size >= size)
			return curr;
		curr = curr->s.next;
	}
	return NULL;
}

void pt_free(void *block)
{
	header_t *header, *tmp;
	/* program break is the end of the process's data segment */
	void *programbreak;

	if (!block)
		return;
	// pthread_mutex_lock(&global_malloc_lock);
	header = (header_t*)block - 1;
	/* sbrk(0) gives the current program break address */
	programbreak = sbrk(0);

	/*
	   Check if the block to be freed is the last one in the
	   linked list. If it is, then we could shrink the size of the
	   heap and release memory to OS. Else, we will keep the block
	   but mark it as free.
	 */
	if ((char*)block + header->s.size == programbreak) {
		if (head == tail) {
			head = tail = NULL;
		} else {
			tmp = head;
			while (tmp) {
				if(tmp->s.next == tail) {
					tmp->s.next = NULL;
					tail = tmp;
				}
				tmp = tmp->s.next;
			}
		}
		/*
		   sbrk() with a negative argument decrements the program break.
		   So memory is released by the program to OS.
		*/
		sbrk(0 - header->s.size - sizeof(header_t));
		/* Note: This lock does not really assure thread
		   safety, because sbrk() itself is not really
		   thread safe. Suppose there occurs a foregin sbrk(N)
		   after we find the program break and before we decrement
		   it, then we end up realeasing the memory obtained by
		   the foreign sbrk().
		*/
		//pthread_mutex_unlock(&global_malloc_lock);
		// pt_mem_used -= header->s.size + sizeof(header_t);
		return;
	}
	header->s.is_free = 1;
	//pt_mem_used -= header->s.size;
	//pthread_mutex_unlock(&global_malloc_lock);
}

void *pt_malloc(size_t size)
{
	size_t total_size;
	void *block;
	header_t *header;
	if (!size)
		return NULL;
	size = align(size);
	//pthread_mutex_lock(&global_malloc_lock);
	header = get_free_block(size);
	if (header) {
		/* Woah, found a free block to accommodate requested memory. */
		// pt_mem_used += header->s.size + sizeof(header_t);
		header->s.is_free = 0;
		//pthread_mutex_unlock(&global_malloc_lock);
		return (void*)(header + 1);
	}
	/* We need to get memory to fit in the requested block and header from OS. */
	total_size = sizeof(header_t) + size ;
	block = sbrk(total_size);
	if (block == (void*) -1) {
		//pthread_mutex_unlock(&global_malloc_lock);
		return NULL;
	}
	// pt_mem_used += total_size;
	header = block;
	header->s.size = size;
	header->s.is_free = 0;
	header->s.next = ((void*)0);
	if (!head)
		head = header;
	if (tail)
		tail->s.next = header;
	tail = header;
	// pthread_mutex_unlock(&global_malloc_lock);
	return (void*)(header + 1);
}

void *pt_calloc(size_t num, size_t nsize)
{
	size_t size;
	void *block;
	if (!num || !nsize)
		return NULL;
	size = num * nsize;
	/* check mul overflow */
	if (nsize != size / num)
		return NULL;
	block = pt_malloc(size);
	if (!block)
		return NULL;
	memset(block, 0, size);
	return block;
}

void *pt_realloc(void *block, size_t size)
{
	header_t *header;
	void *ret;
	if (!block || !size)
		return pt_malloc(size);
	header = (header_t*)block - 1;
	if (header->s.size >= size)
		return block;
	ret = pt_malloc(size);
	if (ret) {
		/* Relocate contents to the new bigger block */
		memcpy(ret, block, header->s.size);
		/* Free the old memory block */
		pt_free(block);
	}
	return ret;
}

void pt_reset_memory()
{
	memset(HEAP,0,HEAP_SIZE);
	pt_mem_index = HEAP;
	pt_mem_used = 0;
}
#endif

uint32_t pt_get_used_memory()
{
	return 0;//pt_mem_used;
}
uint32_t pt_get_free_memory()
{
	return 0;//HEAP_SIZE - pt_mem_used;
}
void print_mem_list()
{
#if 0
	header_t *curr = head;
	/// printf("head = %p, tail = %p \n", (void*)head, (void*)tail);
	printf("=====================[ MEMORY MAP ]===============================\n\r");
	while(curr) {
		printf("index = %50X, size = %zu, is_free=%s\n\r",
			((char*)curr - HEAP),
			curr->s.size, curr->s.is_free ? "FREE" : "USED");
		//printf("\e[107;32m%p\e[107;33m%4zu\e[107;34m %s \e[100;97m ",(void*)curr + sizeof(header_t), curr->s.size, curr->s.is_free ? "FREE" : "USED");
		size_t S = 0;
		for(char *I = (char*)curr + sizeof(header_t);S < curr->s.size; I++, S++)
		{
			if(*I >= ' ' && *I < 0x7f ) printf ("%c", *I);
			else printf ("[%02X] ", *I);
		}
		printf("\e[0m\n\r");
		curr = curr->s.next;
	}
}

int memtest()
{
	printf("ALIGN CHK: %2d %2d %2d %2d\n\r",align(3),align(8),align(12),align(16));
	printf("START : 0x%X  0x%X\n\r", &HEAP[0], sbrk(0) );
	char* CA = pt_malloc(sizeof(char) * 5);
	int* T = pt_malloc(sizeof(int));
	short* S = pt_malloc(sizeof(short));
	char* b = pt_malloc(16); 
	if (b == NULL) printf ("OUT OF MEMORY!?\n\r");
	else strcpy(b,"HELLO WORLD!!!!");
	b = pt_malloc(16);
	if (b == NULL) printf ("OUT OF MEMORY!?\n\r");
	else strcpy(b,"OH FUN DIP!");
	/* char **argv = pt_malloc(sizeof(char**) * c);
	for (int i = 0; i < c; i++)
	{
		argv[i] = pt_malloc(strlen(v[0])+1);
		if (argv[i] == NULL) break;
		strcpy(argv[i],v[i]);
	} */
	*T = 0xFFFFFFFF;
	*S = 0xF00F;
	//printf("MALLOC INT: 0x%X  0x%X\n", &HEAP[0], sbrk(0) );
	if (CA != NULL) { 
		CA[0]='C'; 
		CA[1]='A'; 
		CA[2]='-'; 
		CA[3]='5'; 
		CA[4]='\xff'; 
		// CA[5]='\x00'; // bad buffer overrun 
	}
	//printf("MALLOC CHAR ARRAY: 0x%X  0x%X\n", &HEAP[0], sbrk(0) );
	/* int rc = 0;
	for (int x = 0;x < HEAP_SIZE; x++ )
	{
		// printf ("%c", HEAP[x] == 0 ? '.' : 'X');
		if (HEAP[x] == 0) printf ("-- ");
		else			  printf ("%02X ", HEAP[x]);
		if (++rc >= 16)
		{
			rc = 0;
			printf("\n");
		}
		
	}
	printf("\n"); */
	print_mem_list();
	b = pt_realloc(CA, 20);
	if (b != NULL) {b[3] = '2'; b[4]='0';}
	b = pt_calloc(1,sizeof(int));
	pt_free(T);
	pt_free(CA);
	/* rc = 0;
	for (int x = 0;x < HEAP_SIZE; x++ )
	{
		// printf ("%c", HEAP[x] == 0 ? '.' : 'X');
		if (HEAP[x] == 0) printf ("-- ");
		else			  printf ("%02X ", HEAP[x]);
		if (++rc >= 16)
		{
			rc = 0;
			printf("\n");
		}
		
	} */
	//printf("FREE : 0x%X  0x%X\n", &HEAP[0], sbrk(0) );

	print_mem_list();
#endif
}
