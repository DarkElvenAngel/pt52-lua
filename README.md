# PT52 With LUA

Welcome, this project has been in development for sometime and has evolved along the way.  The PT52 was a serial terminal based on the VT52 there was always a plan to do more it. The PT52 development moved slowly and then it was time for a rethink, new features something that was more usable more inline with an 80's style computer that could do graphics, games, and other tasks.  A search began to find a new interpreter several were considered.  In the end Lua was chosen there was a proof of concept that the RP2040 could run it and with modification and addon libraries it was possible to integrate into the PT52 concept.  Welcome to the PT52-Lua.

## How to use this

PT52 is designed for the **Pimoroni Pico VGA Demo Base** by Pimoroni and the Pico or Pico 2 from Raspberry Pi. With these two items and the correctly install pico SDK follow these steps to get going.  

```text
~ $ git clone https://gitlab.com/DarkElvenAngel/pt52-lua.git
~ $ cd pt52-lua
~/pt52-lua $ git submodules init
~/pt52-lua $ git submodules update
~/pt52-lua $ mkdir build
~/pt52-lua $ cd build
~/pt52-lua/build $ cmake .. && make
```

After the build process completes Flash the `pt52-lua.uf2` to the pico that is then install into the **pico demo**.  Power the **pico demo** via the **USB POWER** port, connect a USB keyboard to the pico's programming port, and the VGA out to your monitor or TV. If you want the full experience a FAT formatted SD card should be inserted into the SD card slot

**NOTE:** to build for the Pico 2 you must either edit the CMakelist.txt and uncomment this line or enter this command before you build `export PICO_PLATFORM=rp2350`

```CMAKE
# set(PICO_PLATFORM rp2350)
```

## Project Status

PT52-Lua is still under development however it's now in a usable state. The basics are all in place and I estimate it's at a late alpha stage now with most key features in place.

This project draws on other projects to work

- Fatfs by ChaN. *BSD LICENSE*
- Lua *MIT LICENSE*
- umm_malloc by Ralph Hempel  *MIT LICENSE*

## NOTE

This project is a work in progress, and very much a passion project.  The goal is to get the basics in place for a boot to Lua computer that isn't unlike the boot to BASIC machines of the past.  A SD card is not required for Lua REPL but to get the full benefits one is highly recommended.  

## Getting started

The PT52-Lua will start up to a command prompt so long as a suitable USB keyboard is detected. Without the SD card the only usable command is `lua` otherwise the `help` command offers up the basic commands available from the shell.

## Reserved paths

PT52-Lua does have basic disk command and any SD Card should have the following folders added.

- `/system` This is where system scripts should be stored if marked executable *(the system flag is set)* then they can be called from the command prompt.
- `/libs`   This is where you can store Lua Libraries, each library should be stored in it's named subfolder and contain the file `init.lua`

## Pico Terminal SHell

`PTSH` is the user interface that allows you to enter commands and run program.  There are some important things to know about the shell,

- has no pipes you cannot redirect the output of a script to another
- has no wild cards
- has limited support for environment variable ***see below***
- uses an execution flag to control if a file should be run *(the system flag is set)*
- use `!` in front of a filename will run that file regardless of the execution flag

`PTSH` has several commands built in these are

- `cd` Change current director of print current path
- `mem` Print free memory *this doesn't show HEAP Memory for user programs*
- `cls` Clear screen
- `exit` does nothing this was used in batch files which are not supported
- `edit` this will launch the **Femto** line editor my `ed` like editor
- `ls` List directory
- `type` print the contents of file to screen
- `help` prints this list of commands
- `ver` print the version number of ptsh
- `mkdir` make a directory
- `move` move or rename a file
- `copy` copy a file
- `del` delete a file
- `lua` launch Lua REPL
- `luac` Lua compiler compiles scripts to bytecode
- `attrib` print file stats or set file flags
- `set` Sets or changes an environment variable
- `unset` clears an environment variable
- `reboot` reboot the PT52

`PTSH` file commands are basic Lua can be use to write more feature rich ones and even replace the defaults.  The program executer uses the **PATH** environment variable and current working directory then in the builtin command list so long as the system flag is set then the script will run.  PTSH uses FATFS and remaps the system flag for the execute flag. setting this flag makes your script run with out calling with `!` or `lua`

### PTSH Changes in ver 0.0.6

New in PTSH is the ability to use environment variables and quotes in the command line. This is useful if you want to customize your **PATH** for example.  Currently **PATH** is the only variable that effects the shell in a meaningful way.  The default limits set on environment variables are names can be up 19 characters long and the values up to 100, with only 10 slots available.

Shell commands can now pass a variable as an argument or the command name.  Simply use the `$` followed by the name. Example `$PATH`.

There are also two special variables `$!` Last command and `$?` exit code

PTSH calls `/autoexec` if it's present and the execution bit is set this will run as a lua script on each boot.  This is where you can customize your start up.  *see Lua for restrictions*

## LUA

The PT52-Lua has Lua version 5.4.7 built in and has been modified to run on the pico and PT52 system. Unlike tradition Lua no libraries are loaded by default due to the limit memory on the pico.  All of the core libraries can be loaded by use of the `uses(library)` function the table for that library is returned as a global.  Likewise the `require()` function isn't available unless the `package` library is loaded first.

### PT-Lua's core libraries

- `package` This contains `require()` and package table of functions
- `coroutine`
- `table`
- `io` The PT52 lacks the **stdout**, **stdin**, & **stderr** these are removed
- `os` The functions to run shell commands are boxed due to system limitations
- `string`
- `math`
- `uft8` This library may not be useful since the PT52 uses ASCII
- `debug`
- `pt` Contains many functions to access system resources
- `draw` Graphics mode and simple drawing tool
- `sprite` Sprite functions
- `vram` Video Memory functions ***SEE VRAM BELOW***
- `fs` File System function missing from io and os
- `uart` Functions for the UART
- `overlay` Functions for working with the graphic overlay

For more see [Lua Libraries](LuaLibs.md)

## Display VRAM

The VRAM is fully accessible with the `vram` library this is the most powerful of the libraries and most functions of the `pt` and all functions in the `draw`, `sprite`, and `overlay` libraries can be done with pure Lua and this library.  As a consequence this can cause undesired behavior or system crashes use of this library requires caution.

### Display Modes

- MODE_TXT,       28 lines of text plus 2 lines used for Header and Footer  
- MODE_T2,        25 lines of text uses Font Buffer 32 colours  
- MODE_G0,        High Resolution Monochrome Mode 640 x 200
- MODE_G1,        4  Colour Mode   320 x 200 user defined pallet 2bpp
- MODE_G2,        16  Colour Mode  320 x 100 user defined pallet 4bpp
- MODE_G3,        256 Colour Mode  160 x 100 user defined pallet 8bpp
- MODE_TILE       This mode is reserved and a placeholder
- MODE_BLANK      Blank screen This mode will only render the Overlay if enabled

Display modes T2, G0 - G3, Tile and Blank  all have a Text Header, Footer and Status Bar.

Sprites or Overlay will render on G0 - G3, Tile and Blank. T2 only supports Sprites. Note that Sprites and overlay **cannot** be used at the same time.

An example of the overlay can be seen during boot. The Overlay is a 160x100 4bpp frame buffer independent of the main buffer.

### Memory Layout

|                       | size (bytes) | size (kilobytes) | Start | End |  
| --------------------- | --------- | ----------- | ---------- | ------ |
| Text mode 1           |8960  bytes|      8.75  k|    0x0000  | 0x2300 |  
| Text Mode 2           |4000  bytes|             |    0x0000  | 0x0F9F |
| Graphical             |16000 bytes|     15.625 k|    0x0000  | 0x3E7F |  
| Display Registers     |384   bytes|      0.375 k|    0x3E80  | 0x3FFF |  
| CLUT                  |512   bytes|       .5   k|    0x4000  | 0x41FF |
| Sprite CLUT           |32    bytes|             |    0x4200  | 0x421F |
| Sprite Registers      |128   bytes|             |    0x4220  | 0x429F |
| Sprite Data           |4096  bytes|      4     k|    0x42A0  | 0x529F |
| Font Buffer           |2048  bytes|      2     k|    0x52A0  | 0x5A9F |
| Extended Font Buffer  |352   bytes|             |    0x5AA0  | 0x5BFF |
| Overlay Frame Buffer  |8000  bytes|             |    0x5C00  | 0x7B3F |
| Overlay CLUT          |32    bytes|             |    0x7B40  | 0x7B7F |

**NOTE:** *Text and graphical buffers all share the same memory space*  

#### Text mode 1 Memory Block

32 bits `|xxffieru|FFFFFFFF|BBBBBBBB|CCCCCCCC|`  
x - *reserved*  
u - underline  
r - reverse *(FLAG)*  
e - extended  
i - intensity *(FLAG)*  
f - font 2 bits  
B - background colour  
F - Foreground colour  
C - Character  

#### Text mode 2 Memory Block

16 bits `|BBBB:FFFF|CCCCCCCC|`  
B - background colour  
F - Foreground colour  
C - Character  

#### Display Registers

| **Register Name**            | **Bit(s)** | **Index** | **MASK** |
| ---------------------------- | ---------- | --------- | -------- |
| _Display_Mode                | 3          | 0         | 0x07     |
| _Display_Flip                | 1          | 0         | 0x08     |
| *UNUSED*                     | 4          | 0         | 0xf0     |
| **Cursor**                   | ---------- | --------- | -------- |
| Column                       | 8          | 1         | 0xff     |
| Line                         | 8          | 2         | 0xff     |
| underline                    | 1          | 3         | 0x01     |
| reverse                      | 1          | 3         | 0x02     |
| extended                     | 1          | 3         | 0x04     |
| intensity                    | 1          | 3         | 0x08     |
| Use_Font                     | 2          | 3         | 0x30     |
| foreground_colour            | 8          | 4         | 0xff     |
| background_colour            | 8          | 5         | 0xff     |
| Pallet                       | 8          | 6         | 0xff     |
| **Stored Cursor**            | ---------- | --------- | -------- |
| Column                       | 8          | 7         | 0xff     |
| Line                         | 8          | 8         | 0xff     |
| underline                    | 1          | 9         | 0x01     |
| reverse                      | 1          | 9         | 0x02     |
| extended                     | 1          | 9         | 0x04     |
| intensity                    | 1          | 9         | 0x08     |
| Use_Font                     | 2          | 9         | 0x30     |
| foreground_colour            | 8          | 10        | 0xff     |
| background_colour            | 8          | 11        | 0xff     |
| Pallet                       | 8          | 12        | 0xff     |
| **Character Buffer Flags**   | ---------- | --------- | -------- |
| Line_Wrap                    | 1          | 13        | 0x01     |
| Cr_Lf                        | 1          | 13        | 0x02     |
| Origin_mode                  | 1          | 13        | 0x04     |
| PT52_mode                    | 1          | 13        | 0x08     |
| *UNUSED*                     | 4          | 13        | 0xf0     |
| Top_Margin                   | 8          | 14        | 0xff     |
| Bottom_Margin                | 8          | 15        | 0xff     |
| Lines                        | 8          | 16        | 0xff     |
| Columns                      | 8          | 17        | 0xff     |
| **Display Flags**            | ---------- | --------- | -------- |
| Cursor_Blink                 | 1          | 18        | 0x01     |
| _Blink                       | 1          | 18        | 0x02     |
| led_state                    | 1          | 18        | 0x04     |
| KeyBoard_Mounted             | 1          | 18        | 0x08     |
| LAMPS                        | 4          | 18        | 0xf0     |
| keylock_flags                | 8          | 19        | 0xff     |
| **Last Key Input**           | ---------- | --------- | -------- |
| last_char                    | 8          | 20        | 0xff     |
| ctrl                         | 1          | 21        | 0x01     |
| shift                        | 1          | 21        | 0x02     |
| alt                          | 1          | 21        | 0x04     |
| meta                         | 1          | 21        | 0x08     |
| *UNUSED*                     | 4          | 21        | 0xf0     |
| ASCII Character              | 8          | 22        | 0xff     |
| Key Code                     | 8          | 23        | 0xff     |
| **Keyboard Flags**           | ---------- | --------- | -------- |
| _KEYBOARD_FN_KEYS            | 1          | 24        | 0x01     |
| _KEYBOARD_CTRL_C             | 1          | 24        | 0x02     |
| _KEYBOARD_LOCAL_ECHO         | 1          | 24        | 0x04     |
| **Current Keys**             | ---------- | --------- | -------- |
| _KEYBOARD_MOD_KEYS           | 8          | 25        | 0xff     |
| _KEYBOARD_KEY_CODE_0         | 8          | 26        | 0xff     |
| _KEYBOARD_KEY_CODE_1         | 8          | 27        | 0xff     |
| _KEYBOARD_KEY_CODE_2         | 8          | 28        | 0xff     |
| _KEYBOARD_KEY_CODE_3         | 8          | 29        | 0xff     |
| _KEYBOARD_KEY_CODE_4         | 8          | 30        | 0xff     |
| _KEYBOARD_KEY_CODE_5         | 8          | 31        | 0xff     |
| **Header Footer Status Bar** | ---------- | --------- | -------- |
| HEADER_FG                    | 4          | 32        | 0x0f     |
| HEADER_BG                    | 4          | 32        | 0xf0     |
| HEADER[80]                   | 640        | 33 - 112  |          |
| FOOTER_FG                    | 4          | 113       |          |
| FOOTER_BG                    | 4          | 113       |          |
| FOOTER[80]                   | 640        | 114 - 193 |          |
| STATUS[80]                   | 640        | 194 - 273 |          |
| HEAD_ENABLE                  | 1          | 274       | 0x01     |
| HEAD_FONT                    | 2          | 274       | 0x06     |
| FOOT_ENABLE                  | 1          | 274       | 0x08     |
| FOOT_FONT                    | 2          | 274       | 0x30     |
| STAT_ENABLE                  | 1          | 274       | 0x40     |
| STAT_USER                    | 1          | 274       | 0x80     |
| HEAD_Pallet                  | 3          | 275       | 0x07     |
| FOOT_Pallet                  | 3          | 275       | 0x38     |
| HSR_RES                      | 2          | 275       | 0xc0     |
| **OVERLAY**                  | ---------- | --------- | -------- |
| OVERLAY_X                    | 8          | 276       |          |
| OVERLAY_Y                    | 8          | 277       |          |
| OVERLAY_W                    | 8          | 278       |          |
| OVERLAY_H                    | 8          | 279       |          |
| OVERLAY_CLUT                 | 2          | 280       | 0x03     |
| OVERLAY_X_SCALE              | 1          | 280       | 0x04     |
| OVERLAY_Y_SCALE              | 1          | 280       | 0x08     |
| OVERLAY_ENABLE               | 1          | 280       | 0x10     |
| OVERLAY_TRANSPARENT          | 1          | 280       | 0x20     |
| OVERLAY_PROTECT              | 1          | 280       | 0x40     |
| OVERLAY_RESERVED             | 1          | 280       | 0x80     |
| OVERLAY_CLUT_WIN             | 8          | 281       |          |

## Plans

- [X] Text mode overhaul
- [X] Display needs to use Registers not variables
- [X] Better Lua library to access Video Memory

For a fuller list check the [TODO.md](TODO.md)
