#define modulelib_c
#define LUA_LIB

#include "lprefix.h"


#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "lua.h"

#include "lauxlib.h"
#include "lualib.h"

static const luaL_Reg loadablelibs[] = {
  {LUA_LOADLIBNAME, luaopen_package}, // 0
  {LUA_COLIBNAME, luaopen_coroutine},
  {LUA_TABLIBNAME, luaopen_table},
  {LUA_IOLIBNAME, luaopen_io},
  {LUA_OSLIBNAME, luaopen_os},        // 4
  {LUA_STRLIBNAME, luaopen_string},
  {LUA_MATHLIBNAME, luaopen_math},
  {LUA_UTF8LIBNAME, luaopen_utf8},
  {LUA_DBLIBNAME, luaopen_debug},
  {LUA_PTLIBNAME, luaopen_pt},
  {LUA_DRAWLIBNAME, luaopen_draw},
  {LUA_SPRITESLIBNAME, luaopen_sprites},
  {LUA_VRAMLIBNAME, luaopen_vram},
  {LUA_FSLIBNAME, luaopen_fs},
  {LUA_UARTLIBNAME, luaopen_uart},
  {LUA_OVERLAYLIBNAME, luaopen_overlay},
};

static int mod_uses (lua_State *L) {
  const char *name = luaL_checkstring(L, 1);
  static const char *const opts[] = {LUA_LOADLIBNAME, LUA_COLIBNAME, LUA_TABLIBNAME, LUA_IOLIBNAME, 
    LUA_OSLIBNAME, LUA_STRLIBNAME, LUA_MATHLIBNAME, LUA_UTF8LIBNAME, LUA_DBLIBNAME, LUA_PTLIBNAME, 
    LUA_DRAWLIBNAME, LUA_SPRITESLIBNAME, LUA_VRAMLIBNAME, LUA_FSLIBNAME, LUA_UARTLIBNAME,
    LUA_OVERLAYLIBNAME, NULL };

  int i = luaL_checkoption(L, 1, NULL, opts);
  switch (i)
  {
  //   case 0:{
  //     const luaL_Reg lib = loadablelibs[i];
  //     luaL_requiref(L, lib.name, lib.func, 1);
  //     lua_pushboolean(L, 1);
  //     luaL_dostring(L,"function require(name)\nlocal filename = package.searchpath(name, package.path)\nif (filename) then dofile(filename) end\nend\n");
  // //lua_pop(L, 1);
  //     return 2;
  // } // Package
/*     case 4: // OS this has been disabled and removed
      lua_pushnil(L);
      lua_pushboolean(L, 0);
      return 2; */
    default: {
      const luaL_Reg lib = loadablelibs[i];
      luaL_requiref(L, lib.name, lib.func, 1);
      lua_pushboolean(L, 1);
    }
  }

  
  return 2;
}
static const luaL_Reg module_funcs[] = {
  {"uses", mod_uses},
  {NULL, NULL}
};

LUAMOD_API int luaopen_module (lua_State *L) {
  /* open lib into global table */
  lua_pushglobaltable(L);
  luaL_setfuncs(L, module_funcs, 0);
  return 1;
}