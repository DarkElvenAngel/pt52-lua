/*
** $Id: lualib.h $
** Lua standard libraries
** See Copyright Notice in lua.h
*/


#ifndef lualib_h
#define lualib_h

#include "lua.h"


/* version suffix for environment variable names */
#define LUA_VERSUFFIX          "_" LUA_VERSION_MAJOR "_" LUA_VERSION_MINOR


LUAMOD_API int (luaopen_base) (lua_State *L);

#define LUA_COLIBNAME	"coroutine"
LUAMOD_API int (luaopen_coroutine) (lua_State *L);

#define LUA_TABLIBNAME	"table"
LUAMOD_API int (luaopen_table) (lua_State *L);

#define LUA_IOLIBNAME	"io"
LUAMOD_API int (luaopen_io) (lua_State *L);

#define LUA_OSLIBNAME	"os"
LUAMOD_API int (luaopen_os) (lua_State *L);

#define LUA_STRLIBNAME	"string"
LUAMOD_API int (luaopen_string) (lua_State *L);

#define LUA_UTF8LIBNAME	"utf8"
LUAMOD_API int (luaopen_utf8) (lua_State *L);

#define LUA_MATHLIBNAME	"math"
LUAMOD_API int (luaopen_math) (lua_State *L);

#define LUA_DBLIBNAME	"debug"
LUAMOD_API int (luaopen_debug) (lua_State *L);

#define LUA_LOADLIBNAME	"package"
LUAMOD_API int (luaopen_package) (lua_State *L);

#define LUA_PTLIBNAME	"pt"
LUAMOD_API int (luaopen_pt) (lua_State *L);

#define LUA_DRAWLIBNAME "draw"
LUAMOD_API int (luaopen_draw) (lua_State *L);

#define LUA_SPRITESLIBNAME "sprite"
LUAMOD_API int (luaopen_sprites) (lua_State *L);

#define LUA_VRAMLIBNAME "vram"
LUAMOD_API int (luaopen_vram) (lua_State *L);

#define LUA_MODULELIBNAME "module"
LUAMOD_API int luaopen_module (lua_State *L);
#define LUA_PTLIBNAME	"pt"
LUAMOD_API int (luaopen_pt) (lua_State *L);

#define LUA_DRAWLIBNAME "draw"
LUAMOD_API int (luaopen_draw) (lua_State *L);

#define LUA_SPRITESLIBNAME "sprite"
LUAMOD_API int (luaopen_sprites) (lua_State *L);

#define LUA_VRAMLIBNAME "vram"
LUAMOD_API int (luaopen_vram) (lua_State *L);

#define LUA_MODULELIBNAME "module"
LUAMOD_API int luaopen_module (lua_State *L);

#define LUA_FSLIBNAME "fs"
LUAMOD_API int luaopen_fs (lua_State *L);

#define LUA_UARTLIBNAME "uart"
LUAMOD_API int luaopen_uart (lua_State *L);

#define LUA_OVERLAYLIBNAME "overlay"
LUAMOD_API int luaopen_overlay (lua_State *L);

/* open all previous libraries */
LUALIB_API void (luaL_openlibs) (lua_State *L);


#endif
