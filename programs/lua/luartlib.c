/**
 * @file luartlib.c
 * @author Darkelvenangel
 * @brief UART to Lua via Pico SDK Module
 * @version 0.1
 * @date 2024-08-07
 * 
 * @copyright Copyright (c) 2024
 * 
 */

#define luartlib_c
#define LUA_LIB

#include "pt52io.h"
#include "pifo/pifo.h"

#include "lua.h"

#include "lauxlib.h"
#include "lualib.h"
#include "hardware/clocks.h"
#include "pico/multicore.h"
#include "hardware/uart.h"
#include "hardware/irq.h"

#define UART_ID uart1
#undef BAUD_RATE
#define BAUD_RATE 9600
#define DATA_BITS 8
#define STOP_BITS 1
#define PARITY    UART_PARITY_NONE

static const char *const PARITY_OFFSETS[]   = { "none", "even", "odd", NULL };
#pragma pack(1)
struct uart_context
{
    uint32_t baudrate;
    uint8_t  data_bits : 4;
    uint8_t  stop_bits : 2;
    uint8_t  parity    : 2;
    union {
        struct {
            uint8_t _initialized:1;
            uint8_t _interrupt:1;
        };
        uint8_t flags;
    };
    pifo_t  pifo;
};

struct uart_context UART_Context;

#define UART_pifo &UART_Context.pifo
// HELPER FUNCTION
void Push_Configuration(lua_State *L)
{
    lua_pushinteger(L, UART_Context.baudrate);
    lua_pushinteger(L, UART_Context.data_bits);
    lua_pushinteger(L, UART_Context.stop_bits);
    lua_pushinteger(L, UART_Context.parity);
}
// Lua Call
// uart.init(baud, data_bits, stop_bits, parity, callback)
static int UART_init(lua_State *L)
{
    if (UART_Context._initialized){
        luaL_error(L, "UART is already initalized");
        return 0;
    }  //  UART is already initalized
    // Set up our UART context.
    UART_Context.baudrate  = luaL_optinteger(L, 1, BAUD_RATE);
    UART_Context.data_bits = luaL_optinteger(L, 2, DATA_BITS);
    UART_Context.stop_bits = luaL_optinteger(L, 3, STOP_BITS);
    UART_Context.parity    = luaL_optinteger(L, 4, PARITY);
#ifdef UART_IRQ_ENABLED
    irq_handler_t Uart_RX = NULL;
#endif

    // Get the actual baud rate
    UART_Context.baudrate = uart_init(UART_ID, UART_Context.baudrate);  // returns baud rate
    gpio_set_function(PICO_DEFAULT_UART_TX_PIN, GPIO_FUNC_UART);  // SET GPIO TX PIN FUNCTION
    gpio_set_function(PICO_DEFAULT_UART_RX_PIN, GPIO_FUNC_UART);  // SET GPIO RX PIN FUNCTION

    // Set UART flow control CTS/RTS, we don't want these, so turn them off
    uart_set_hw_flow(UART_ID, false, false);
    
    uart_set_format(UART_ID, UART_Context.data_bits, UART_Context.stop_bits, UART_Context.parity);
    uart_set_fifo_enabled(UART_ID, true);
#ifdef UART_IRQ_ENABLED
    // And set up and enable the interrupt handlers
    if (Uart_RX != NULL)
    {
        irq_set_exclusive_handler(UART1_IRQ, Uart_RX);
        irq_set_enabled(UART1_IRQ, true);
        uart_set_irq_enables(UART_ID, true, false);
    }
#endif
    UART_Context._initialized = true;
    Push_Configuration(L);
    return 4;
}

static int UART_setup(lua_State *L)
{
    if (!UART_Context._initialized){
        luaL_error(L, "UART isn't initalized");
        return 0;
    }  
    UART_Context.baudrate  = luaL_optinteger(L, 1, BAUD_RATE);
    UART_Context.data_bits = luaL_optinteger(L, 2, DATA_BITS);
    UART_Context.stop_bits = luaL_optinteger(L, 3, STOP_BITS);
    UART_Context.parity    = luaL_optinteger(L, 4, PARITY); 
    
    UART_Context.baudrate = uart_set_baudrate(UART_ID, UART_Context.baudrate);
    uart_set_format(UART_ID, UART_Context.data_bits, UART_Context.stop_bits, UART_Context.parity);

    Push_Configuration(L);
    return 4;
}

static int UART_close(lua_State *L)
{
    uart_deinit(UART_ID);
    if (UART_Context._interrupt) {
        UART_Context._interrupt = false;
        irq_set_enabled(UART1_IRQ, false);
        uart_set_irq_enables(UART_ID, false, false);
    }
    UART_Context._initialized = false;
    return 0;
}

void UART_RESET()
{
    UART_close(NULL);
}

static int UART_isReadable(lua_State *L)
{
    if (!UART_Context._initialized){
        luaL_error(L, "UART isn't initalized");
        return 0;
    }  
    if (UART_Context._interrupt) {
        lua_pushboolean(L, pifo_status(UART_pifo) > 0);
    } else {
        lua_pushboolean(L, uart_is_readable(UART_ID));
    }
    return 1;
}

static int UART_getConfig(lua_State *L)
{
    if (!UART_Context._initialized){
        luaL_error(L, "UART isn't initalized");
        return 0;
    }  
    Push_Configuration(L);
    return 4;
}

static int UART_setBaudRate(lua_State *L)
{
    if (!UART_Context._initialized){
        luaL_error(L, "UART isn't initalized");
        return 0;
    }  
    UART_Context.baudrate  = luaL_optinteger(L, 1, BAUD_RATE);
    UART_Context.baudrate = uart_set_baudrate(UART_ID, UART_Context.baudrate);

    lua_pushinteger(L, UART_Context.baudrate);    
    return 1;
}

static int UART_receive(lua_State *L)
{
    if (!UART_Context._initialized){
        luaL_error(L, "UART isn't initalized");
        return 0;
    }
    if (UART_Context._interrupt){
        lua_pushinteger(L, pifo_read(UART_pifo));
    } else {
        if (uart_is_readable(UART_ID)) {lua_pushinteger(L, uart_getc(UART_ID));} else {lua_pushinteger(L, PIFO_EMPTY);}
    }
    return 1;
}

static int UART_send(lua_State *L)
{
    if (!UART_Context._initialized){
        luaL_error(L, "UART isn't initalized");
        return 0;
    }
    switch (lua_type(L,1))
    {
        case LUA_TNUMBER:
        {
            char c = luaL_optinteger(L, 1, 0); 
            uart_putc_raw(UART_ID, c);
            break;
        }
        case LUA_TSTRING:
        {
            const char* s = luaL_optstring(L,1,"");
            uart_puts(UART_ID, s);
            break;
        }
        default:
            luaL_error(L, "Data Type %s is not supported", lua_typename(L,lua_type(L,1)));
    }
    return 0;
}

/* Not implemented 
 static int UART_write()
{
    char *Data   = "";
    int   length = 0;
    uart_write_blocking(UART_ID, Data, length);
}
 */
static int UART_translate_crlf(lua_State *L)
{
    if (!UART_Context._initialized){
        luaL_error(L, "UART isn't initalized");
        return 0;
    }

    bool b = lua_toboolean(L, 1);
    uart_set_translate_crlf(UART_ID, b);
    return 1;
}
static int UART_set_HWfifo(lua_State *L)
{
    if (!UART_Context._initialized){
        luaL_error(L, "UART isn't initalized");
        return 0;
    }

    bool b = lua_toboolean(L, 1);
    uart_set_fifo_enabled(UART_ID, b);
    return 1;
}

void UART_RX_INTERRUPT()
{
    pifo_write(UART_pifo, uart_getc(UART_ID));
}

static int UART_set_RXFIFO(lua_State *L)
{
    if (!UART_Context._initialized){
        luaL_error(L, "UART isn't initalized");
        return 0;
    }

    if (UART_Context._interrupt){
        luaL_error(L, "UART RX FIFO is already setup");
        return 0;
    }
    pifo_init(UART_pifo);
    uart_set_fifo_enabled(UART_ID, false);

    // And set up and enable the interrupt handlers
    irq_set_exclusive_handler(UART1_IRQ, UART_RX_INTERRUPT);
    irq_set_enabled(UART1_IRQ, true);

    uart_set_irq_enables(UART_ID, true, false);
    UART_Context._interrupt = true;
    lua_pushboolean(L, true);
    return 1;
}

static const luaL_Reg uartlib[] = {
    {"init",            UART_init},             // Initialize the UART
    {"close",           UART_close},            // Close the UART
    {"setup",           UART_setup},            // Setup configuration
    {"send",            UART_send},             // Send data
    {"translate_crlf",  UART_translate_crlf},   // Set CR/LF conversion on UART
    {"isReadable",      UART_isReadable},       // Check if data is waiting in the RX FIFO
    {"receive",         UART_receive},          // Receive data
    {"setBaudRate",     UART_setBaudRate},      // Change baud rate 
    {"getConfig",       UART_getConfig},        // Get current configuration
    {"setHWFIFO",       UART_set_HWfifo},       // Set fifo on or off
    {"useRXFIFO",       UART_set_RXFIFO},       // Set RXfifo on
#ifdef UART_IRQ_ENABLED
    {"setRXHandler",    NULL},            // Set receive interrupt handler
#endif
    {NULL, NULL}
};

/* }====================================================== */

LUAMOD_API int luaopen_uart (lua_State *L) {
    luaL_newlib(L, uartlib);
    return 1;
}