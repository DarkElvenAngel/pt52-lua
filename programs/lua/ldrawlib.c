/*
    This is the PT52 drawing Library
*/

#define ldrawlib_c
#define LUA_LIB

#include "pt52io.h"
#include "pico/scanvideo.h"
extern const uint16_t Pallets[5][16]; // include"pallet.h" 
extern const uint16_t Pallets_256[][256];

#include "lua.h"

#include "lauxlib.h"
#include "lualib.h"

#include <string.h>
#include <stdarg.h>
extern char buf_printf[PRINTF_BUFFER_SIZE];

void put_pixel_c(uint16_t x, uint16_t y, uint8_t c)
{
  uint16_t _BYTE;
  uint16_t  _BIT, _MASK, _ROW;
  switch (_Display_Mode)
  {
    case MODE_G0:
      //if (x > 640 || y > 200) goto ERR;
      c &= 1;
      _BYTE = x / 8;
      _BIT  = 7 - x % 8;
      _MASK = 1;
      _ROW  = y * 80;
      break;
    case MODE_G1:
      //if (x > 320 || y > 200) goto ERR;
      c &= 3;
      _BYTE = x / 4;
      _BIT  = 6 - (x % 4 * 2);
      _MASK = 3;
      _ROW  = y * 80;
      break;
    case MODE_G2:
      //if (x > 320 || y > 100) goto ERR;
      c &= 15;
      _BYTE = x / 2;
      _BIT  = 4 - (x % 2 * 4);
      _MASK = 15;
      _ROW  = y * 160;
      break;
    case MODE_G3:
      //if (x > 160 || y > 100) goto ERR;
      _BYTE = x;
      _BIT  = 0;
      _MASK = 255;
      _ROW  = y * 160;
      break;
    default: goto ERR;    
  }
  if ((_BYTE + _ROW) > 15999) goto ERR;
  _VBUFFERS.VIDEO_BUFF[_BYTE + _ROW] &= ~((255 & _MASK) << _BIT);
  _VBUFFERS.VIDEO_BUFF[_BYTE + _ROW] |= (c << _BIT);
  // printf("%d,%d:SET %d = %x\n\r",x,y,_BYTE + _ROW, c << _BIT);
  return;
ERR:
  return;
  //printf("%d,%d:SET %d = %x\n\r",x,y,_BYTE + _ROW, c << _BIT);
  // printf("%d [PUT PIXEL %d %d %d]\n\r",_Display_Mode, x, y, c);
}

void put_char(int x, int y, int c, int fg, int bg)
{
	int i,j,bits;
	for (i = 0; i < GLYPH_HEIGHT; i++) {
	bits = _VBUFFERS.FONT_BUFFER[GLYPH_HEIGHT * c + i];
		for (j = 0; j < GLYPH_WIDTH; j++, bits <<= 1)
			if (bits & 0x80){ 
        put_pixel_c(x+j,  y+i, fg);
			} else {
        if (bg >= 0) put_pixel_c(x+j,  y+i, bg);
      }
		}
}

void put_string(int x, int y, const char *s, int fg, int bg)
{
	int i;
	for (i = 0; *s; i++, x += GLYPH_WIDTH, s++)
	put_char (x, y, *s, fg, bg);
}

void put_printf(uint16_t *x, uint8_t *y, uint8_t colour, const char *format, ...)
{
  va_list args;
  va_start(args, format);
  vsprintf(buf_printf, format, args);
  va_end(args);
  uint8_t Origin_colour = colour;
  uint8_t Origin_x = *x; 
  uint8_t Font = 0;
  uint16_t DISPLAY_WIDTH,DISPLAY_HEIGHT;
  switch (_Display_Mode)
  {
    case MODE_G0: DISPLAY_WIDTH = 640; DISPLAY_HEIGHT = 200; break;
    case MODE_G1: DISPLAY_WIDTH = 320; DISPLAY_HEIGHT = 200; break;
    case MODE_G2: DISPLAY_WIDTH = 320; DISPLAY_HEIGHT = 100; break;
    case MODE_G3: DISPLAY_WIDTH = 160; DISPLAY_HEIGHT = 100; break;
    default : return;
  }
  for (char *Ch = buf_printf; *Ch != 0; *Ch++)
  {
    switch (*Ch)
    {
      case '\n': {*y += 8; continue;} break;
      case '\r': {*x = Origin_x; continue;} break;
      case '\t': {*x += 18; continue;} break;
      case 27:   {
        Ch++;
        switch (*Ch)
        {
          case 0: return;
          case '~':
            Ch++;
            if ('0' <= *Ch && *Ch <= '9') {
              colour = *Ch - '0';
              continue;
            } else if ('A' <= *Ch && *Ch <= 'F') {
              colour = 10 + (*Ch - 'A');
              continue;
            } else if (*Ch == 'G') {
              colour = Origin_colour;
              continue;
            } 
          break;
#ifdef DRAW_PRINTF_FONT_SUPPORT
          case '#': // Font
            Ch++;
            if ('0' <= *Ch && *Ch <= '4') {
              Font = *Ch - '0';
              continue;
            } 
          break;
#endif
        }
      }
      break; 
      case ' ':  {*x += 6; continue;} break;
    }
    if (*x > DISPLAY_WIDTH - 8)
    {
          *x = Origin_x;
          *y += 8;
    }
    if (*y >= DISPLAY_HEIGHT) break;
    put_char(*x, *y, *Ch >= 32 ? *Ch : 0, colour, -1);
    *x+= 8;
  }
  memset(buf_printf,0,PRINTF_BUFFER_SIZE);
}

static int DRAW_clear_screen(lua_State *L) {
  // uint8_t c = luaL_optinteger(L, 1, 0);
  memset(_VBUFFERS.VIDEO_BUFF,0,16000);
  return 0;
}
static int DRAW_line(lua_State *L) {
  uint16_t x0 = luaL_optinteger(L, 1, 0);
  uint8_t  y0 = luaL_optinteger(L, 2, 0);
  uint16_t x1 = luaL_optinteger(L, 3, 0);
  uint8_t  y1 = luaL_optinteger(L, 4, 0);
  uint8_t  c = luaL_optinteger(L, 5, 0);
  draw_line(put_pixel_c, x0,  y0,  x1,  y1,  c);
  return 0;
}
static int DRAW_rect(lua_State *L) {
  uint16_t x = luaL_optinteger(L, 1, 0);
  uint8_t  y = luaL_optinteger(L, 2, 0);
  uint16_t w = luaL_optinteger(L, 3, 0);
  uint8_t  h = luaL_optinteger(L, 4, 0);
  uint8_t  c = luaL_optinteger(L, 5, 0);
  uint8_t  f = luaL_optinteger(L, 6, -1);
  if (f >= 0) fill_rect(put_pixel_c, x,y,w,h,f);
  draw_rect(put_pixel_c, x,  y,  w,  h,  c);
  return 0;
}
static int DRAW_roundrect(lua_State *L) {
  uint16_t x = luaL_optinteger(L, 1, 0);
  uint8_t  y = luaL_optinteger(L, 2, 0);
  uint16_t w = luaL_optinteger(L, 3, 0);
  uint8_t  h = luaL_optinteger(L, 4, 0);
  uint8_t  r = luaL_optinteger(L, 5, 0); 
  uint8_t  c = luaL_optinteger(L, 6, 0); 
  drawRoundRect(put_pixel_c, x,  y,  w,  h,  r,  c);
  return 0;
}
static int DRAW_circle(lua_State *L) {
  uint16_t x = luaL_optinteger(L, 1, 0);
  uint8_t  y = luaL_optinteger(L, 2, 0);
  uint16_t r = luaL_optinteger(L, 3, 0);
  uint8_t  c = luaL_optinteger(L, 4, 0);
  uint8_t  f = luaL_optinteger(L, 5, -1);
  if (f >= 0) fill_circle(put_pixel_c, x,y,r,f);
  draw_circle(put_pixel_c, x,  y,  r,  c);
  return 0;
}
static int DRAW_ellipse(lua_State *L) {
  uint16_t xc = luaL_optinteger(L, 1, 0);
  uint8_t  yc = luaL_optinteger(L, 2, 0);
  uint16_t rx = luaL_optinteger(L, 3, 0);
  uint16_t ry = luaL_optinteger(L, 4, 0);
  uint8_t  c  = luaL_optinteger(L, 5, 0);
  draw_ellipse(put_pixel_c, xc,  yc,  rx,  ry,  c);//int xc, int yc, int rx, int ry, int c)
  return 0;
}
static int DRAW_pixel(lua_State *L) {
  uint16_t x = luaL_optinteger(L, 1, 0);
  uint8_t  y = luaL_optinteger(L, 2, 0);
  uint8_t  c = luaL_optinteger(L, 3, 0);
  put_pixel_c(x,  y,  c);
  return 0;
}
static int DRAW_mode(lua_State *L) {
  uint16_t m = luaL_optinteger(L, 1, 0);
  switch (m){
    case 0: _Display_Mode = MODE_G0; DRAW_clear_screen(L); break;
    case 1: _Display_Mode = MODE_G1; DRAW_clear_screen(L); break;
    case 2: _Display_Mode = MODE_G2; DRAW_clear_screen(L); break;
    case 3: _Display_Mode = MODE_G3; DRAW_clear_screen(L); break;
    default:_Display_Mode = MODE_TXT; Reset_Character_Buffer();
  }

  return 0;
}
static int DRAW_write_mem(lua_State *L) {
  uint16_t i = luaL_optinteger(L, 1, 0);
  uint8_t  d = luaL_optinteger(L, 2, 0);
  if (i > 16000) return 0;
  _VBUFFERS.VIDEO_BUFF[i] = d;
  return 0;
}
static int DRAW_read_mem(lua_State *L) {
  uint16_t i = luaL_optinteger(L, 1, 0);
  if (i > 16000) {
    luaL_error(L,"OUT OF BOUNDS");
    return 0;
  }
  lua_pushinteger(L, _VBUFFERS.VIDEO_BUFF[i]);
  return 1;
}
static int DRAW_char(lua_State *L) {
  uint16_t x = luaL_optinteger(L, 1, 0);
  uint8_t  y = luaL_optinteger(L, 2, 0);
  uint8_t  c = luaL_optinteger(L, 3, 0);
  uint16_t f = luaL_optinteger(L, 4, 1);
  int16_t  b = luaL_optinteger(L, 5, -1);
  put_char(x,  y,  c,  f,  b);
  return 0;
}
static int DRAW_string(lua_State *L) {
  uint16_t    x = luaL_optinteger(L, 1, 0);
  uint8_t     y = luaL_optinteger(L, 2, 0);
  const char *s = luaL_optstring(L, 3, "");
  uint16_t    f = luaL_optinteger(L, 4, 1);
  int16_t     b = luaL_optinteger(L, 5, -1);
  put_string(x,  y,  s,  f,  b);
  return 0;
}
static int DRAW_print(lua_State *L) {
  uint16_t      x = luaL_optinteger(L, 1, 0);
  uint8_t       y = luaL_optinteger(L, 2, 0);
  uint8_t       c = luaL_optinteger(L, 3, 0);
  const char* str = luaL_optstring(L, 4, "");
  put_printf(&x, &y, c, "%s", str);
  lua_pushinteger(L, x);
  lua_pushinteger(L, y);
  return 2;
}
static int DRAW_CLUT(lua_State *L) {
  uint8_t  i = luaL_optinteger(L, 1, 0);
  uint8_t  r = luaL_optinteger(L, 2, 0);
  uint8_t  g = luaL_optinteger(L, 3, 0);
  uint8_t  b = luaL_optinteger(L, 4, 0);
  if (i > 255) {
    luaL_error(L,"OUT OF BOUNDS");
    return 0;
  }
  _VBUFFERS.CLUT[i] = PICO_SCANVIDEO_PIXEL_FROM_RGB8(r,g,b);
  return 0;
}
static int DRAW_LOAD_CLUT(lua_State *L) {
  uint8_t  i = luaL_optinteger(L, 1, 0);
  uint8_t  e = luaL_optinteger(L, 2, 0);
  if (e == 0){
    if (i > 4) {
      luaL_error(L,"OUT OF BOUNDS");
      return 0;
    }
    memcpy(_VBUFFERS.CLUT,Pallets[i],sizeof(uint16_t) * 16);
    return 0;
  }
  if (i > 3) {
      luaL_error(L,"OUT OF BOUNDS");
      return 0;
    }
    memcpy(_VBUFFERS.CLUT,Pallets_256[i],sizeof(uint16_t) * 256);
  return 0;
}
/* static int DRAW_Load_image(lua_State *L) {
  const char *s = luaL_optstring(L, 1, "");
  FILE* F = fopen(s,"r");
  if (F == NULL) return 0;
  // fread(F,)
  fclose(F);
} */

static const luaL_Reg drawlib[] = {
  {"clr",       DRAW_clear_screen},
  {"line",      DRAW_line},
  {"rect",      DRAW_rect},
  {"roundrect", DRAW_roundrect},
  {"circle",    DRAW_circle},
  {"ellipse",   DRAW_ellipse},
  {"pixel",     DRAW_pixel},
  {"mode",      DRAW_mode},
  {"write",     DRAW_write_mem},
  {"vpoke",     DRAW_write_mem},
  {"vpeek",     DRAW_read_mem},
  {"char",      DRAW_char},
  {"string",    DRAW_string},
  {"print",     DRAW_print},
  {"set_colour",DRAW_CLUT},
  {"pallet",    DRAW_LOAD_CLUT},
  {NULL, NULL}
};

/* }====================================================== */



LUAMOD_API int luaopen_draw (lua_State *L) {
    luaL_newlib(L, drawlib);
    return 1;
}
