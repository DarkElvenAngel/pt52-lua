/*
    # VRAM Library for PT52-Lua

    The purpose of this library is to give Lua access to the VRAM used by the VGA Driver
    
    ## TODO:

    -[X] Basic Read/Write of all VRAM
    -[X] Selectable Ranges for VRAM Operations
    -[X] Allow a table of data to be written in one operation
    -[X] Bulk Read data into a table
    -[ ] Read/Write 16/32/64 bit numbers
    -[ ] Register Functions 
    -[X] Memory operations (save/load) Blocks
    -[ ] Font ROM operations 

    ## Memory Map

    | Graphical             | 16000 bytes | 0x0000 | 0x3E7F |  
    | Display Registers     | 384   bytes | 0x3E80 | 0x3FFF |  
    | CLUT\*                | 512   bytes | 0x4000 | 0x41FF |
    | Sprite CLUT\*         | 32    bytes | 0x4200 | 0x421F |
    | Sprite Registers      | 128   bytes | 0x4220 | 0x429F |
    | Sprite Data           | 4096  bytes | 0x42A0 | 0x529F |
    | Font Buffer           | 2048  bytes | 0x52A0 | 0x5A9F |

    \* CLUT data is 16bits wide

    ## Register Maps

    ### Display

    TBD

    ### Sprite

    Sprite Registers are 64bits wide and there are 16 of them

    uint64_t h:8;               // Height
    uint64_t w:8;               // Width
    int64_t  x:11;              // Postion x
    int64_t  y:9;               // Postion y
    uint64_t colour_depth:2;    // Colour Depth
    uint64_t data_offset:13;    // Offset in sprite buffer
    uint64_t data_length:6;     // Length of sprite data
    uint64_t pallet_shift:4;    // Length of sprite data
    uint64_t scale_x:1;         // Scale x
    uint64_t scale_y:1;         // Scale y
    uint64_t use_all_colours:1; // No Transparency
*/


#define lvramlib_c
#define LUA_LIB

#include "pt52io.h"

#include "lua.h"

#include "lauxlib.h"
#include "lualib.h"

#define SWAP_UINT16(x) (((x) >> 8) | ((x) << 8))
#define SWAP_UINT32(x) (((x) >> 24) | (((x) & 0x00FF0000) >> 8) | (((x) & 0x0000FF00) << 8) | ((x) << 24))

/* 
static const char *const VRAM_OFFSETS[]   = { "all",  "frame", "reg",  "clut", "pclut", "sreg", "sprite", "font", "ufont", NULL };
static const uint16_t VRAM_START_INDEX[]  = { 0x0000, 0x0000,  0x3E80, 0x4000, 0x4200,  0x4220, 0x42A0,   0x52A0, 0x5AA0 ,0     };
static const uint16_t VRAM_LENGTH_INDEX[] = { 23552,  16384,   384,    512,    32,      128,    4096,     2048,   352    ,0     };
 */

struct VRAM_T {
  char*    Base;
  uint16_t Index;
  uint16_t Length;
  bool     R_W;
};

static const char *const VRAM_OFFSETS[]   = { "all",  "frame", "reg",  "clut", "sclut", "sreg", "sprite", "font", "ufont", "overlay", "oclut", "rom", "rom 0", "rom 1", "rom 2", "rom 3", NULL};
static const struct VRAM_T VRAM_MMAP[] = {
  {_VBUFFERS.BUFFER_RAW, 0x0000, 31552, 1}, // all ram
  {_VBUFFERS.BUFFER_RAW, 0x0000, 16000, 1}, // frame buffer
  {_VBUFFERS.BUFFER_RAW, 0x3E80,   384, 1}, // registers
  {_VBUFFERS.BUFFER_RAW, 0x4000,   512, 1}, // colour look up table
  {_VBUFFERS.BUFFER_RAW, 0x4200,    32, 1}, // sprite colour look up table
  {_VBUFFERS.BUFFER_RAW, 0x4220,   128, 1}, // sprite registers
  {_VBUFFERS.BUFFER_RAW, 0x42A0,  4096, 1}, // sprite data buffer
  {_VBUFFERS.BUFFER_RAW, 0x52A0,  2048, 1}, // font buffer
  {_VBUFFERS.BUFFER_RAW, 0x5AA0,   252, 1}, // user font buffer
  {_VBUFFERS.BUFFER_RAW, 0x5C00,  8000, 1}, // overlay frame buffer
  {_VBUFFERS.BUFFER_RAW, 0x7B40,  32,   1}, // overlay clut
  {(char*)font_rom     , 0x0000,  8192, 0}, // font ROM
  {(char*)font_rom     , 0x0000,  2048, 0}, // font ROM bank 0
  {(char*)font_rom     , 0x0800,  2048, 0}, // font ROM bank 1
  {(char*)font_rom     , 0x1000,  2048, 0}, // font ROM bank 2
  {(char*)font_rom     , 0x1800,  2048, 0}, // font ROM bank 3
};

void memset4(uint32_t *dest, uint32_t data, uint32_t n);
void memset2(uint16_t *dest, uint16_t data, uint32_t n);

typedef struct {
  uint8_t *ptr;
  uint8_t Base_Offset;
  uint16_t Offset;
} _VRAM_t;

static _VRAM_t VRAM_HELPER_get_address(lua_State *L, uint8_t *index)
{
  _VRAM_t ret = {NULL,0,0};
  switch (lua_type(L, *index))
  {
    case LUA_TNUMBER:
        ret.Offset = luaL_optinteger(L, *index, 0);
        *index += 1;
        break;
    case LUA_TSTRING:
        ret.Base_Offset = luaL_checkoption(L, *index, VRAM_OFFSETS[0], VRAM_OFFSETS);
        ret.Offset = luaL_optinteger(L, *index + 1, 0);
        *index += 2;
        break;
    default:
        luaL_error(L, "Invalid index type");
  }
  if (ret.Offset < VRAM_MMAP[ret.Base_Offset].Length) 
    ret.ptr = VRAM_MMAP[ret.Base_Offset].Base + VRAM_MMAP[ret.Base_Offset].Index + ret.Offset;
  return ret;
}
static uint8_t VRAM_HELPER_Check_Limit(_VRAM_t *address, uint16_t Length)
{
  if (address->ptr == NULL) return false;
  if (VRAM_MMAP[address->Base_Offset].Length > address->Offset + Length) return true;
  address->ptr = NULL;
  return false;
}
static uint8_t VRAM_HELPER_get_uint8(lua_State *L, uint8_t *index)
{
  switch (lua_type(L, *index))
  {
    case LUA_TNUMBER:
        return luaL_optinteger(L, *index++ , 0);
    case LUA_TSTRING:
        return luaL_optstring(L, *index++, "")[0];
    default:
        luaL_error(L, "Invalid data type");
  }
  return 0;
}

static uint32_t VRAM_HELPER_get_uint32(lua_State *L, uint8_t *index)
{
  switch (lua_type(L, *index))
  {
    case LUA_TNUMBER:
        return luaL_optinteger(L, *index++ , 0);
    case LUA_TSTRING:
    default:
        luaL_error(L, "Invalid data type");
  }
  return 0;
}
static uint8_t VRAM_HELPER_is_aligned(void* ptr, size_t alignment) {
    return ((uintptr_t)ptr % alignment) == 0;
}
static int VRAM_write_mem8(lua_State *L){
  uint8_t p = 0;
  uint16_t i = 0;
  uint8_t  d = 0;
  uint16_t l = 0;
  uint8_t ti = 0;
  switch (lua_type(L,1))
  {
    case LUA_TNUMBER:
        i = luaL_optinteger(L, 1, 0);
        if (lua_type(L,2) == LUA_TTABLE)
        {
          lua_settop(L, 2);
          l = lua_rawlen(L, 2);
          if (l == 0) return 0;
          ti = 2;
        } else {
          d = luaL_optinteger(L, 2, 0);
        }
        break;
    case LUA_TSTRING:
        p = luaL_checkoption(L, 1, VRAM_OFFSETS[0], VRAM_OFFSETS);
        i = luaL_optinteger(L, 2, 0);
        if (lua_type(L,3) == LUA_TTABLE)
        {
          lua_settop(L, 3);
          l = lua_rawlen(L, 3);
          if (l == 0) return 0;
          ti = 3;
        } else {
          d = luaL_optinteger(L, 3, 0);
        }
        break;
    default:
        luaL_error(L, "Invalid index type");
  }
  if (i > VRAM_MMAP[p].Length || i + (l - 1) > VRAM_MMAP[p].Length) 
  {
    luaL_error(L, "Attempt to write to %s beyond the limit of %d", VRAM_OFFSETS[p], VRAM_MMAP[p].Length);
    return 0;
  }
  if (!VRAM_MMAP[p].R_W)
  {
    luaL_error(L,"ERROR: The memory at \"%s\" is read-only", VRAM_OFFSETS[p]);
    lua_pushboolean(L, false);
    return 1;
  }
  if (l != 0)
  {
    for (int x = 1; x <= l; x++) {
        lua_pushinteger(L, x);
        lua_gettable(L, ti); 

        if (lua_isnil(L, -1)) { // relative indexing for "locals"
            l = x-1; // fix actual size (e.g. 4th nil means a_size==3)
            break;
        }

        if (!lua_isnumber(L, -1)) // optional check
            return luaL_error(L, "item %d invalid (number required, got %s)",
                              x, luaL_typename(L, -1));

        d = lua_tointeger(L, -1);

        *(VRAM_MMAP[p].Base + VRAM_MMAP[p].Index + i + (x - 1)) = d;

        lua_pop(L, 1);
    }
  } else *(VRAM_MMAP[p].Base + VRAM_MMAP[p].Index + i) = d;
  return 0;
}

static int VRAM_read_mem8(lua_State *L){
  uint8_t  p = 0;
  uint16_t i = 0;
  uint8_t  l = 1;
  switch (lua_type(L,1))
  {
    case LUA_TNUMBER:
        i = luaL_optinteger(L, 1, 0);
        l = luaL_optinteger(L, 2, 1);
        break;
    case LUA_TSTRING:
        p = luaL_checkoption(L, 1, VRAM_OFFSETS[0], VRAM_OFFSETS);
        i = luaL_optinteger(L, 2, 0);
        l = luaL_optinteger(L, 3, 1);
        break;
    default:
        luaL_error(L, "Invalid index type");
  }
  if (l == 0) return 0;
  if (i > VRAM_MMAP[p].Length || i + (l - 1) > VRAM_MMAP[p].Length)
  {
    luaL_error(L, "Attempt to write from %s beyond the limit of %d", VRAM_OFFSETS[p], VRAM_MMAP[p].Length);
    return 0;
  }
  if (l == 1)
  {
    lua_pushinteger(L, *(VRAM_MMAP[p].Base + VRAM_MMAP[p].Index + i));
  } else {
    lua_newtable(L);
    for (int x = 0; x < l; x++)
    {
      lua_pushinteger(L, x + 1);
      lua_pushinteger(L, *(VRAM_MMAP[p].Base + VRAM_MMAP[p].Index + i + x));
      lua_settable(L, -3);
    }
  }
  return 1;
}

/// search({ADDRESS}, Char, Length)
static int VRAM_memchr(lua_State *L){
  uint8_t index = 1;
  _VRAM_t address = VRAM_HELPER_get_address(L, &index);
  uint8_t data = VRAM_HELPER_get_uint8(L, &index);  // Cannot be a table
  uint16_t length = luaL_optinteger(L, index++, 1);
  if (length == 0) return 0;
  if (!VRAM_HELPER_Check_Limit(&address, length)) { luaL_error(L, "Attempt to access outside memory range"); return 0; }
  uint8_t *c = memchr(address.ptr, data, length); 
  if (c == NULL) lua_pushnil(L);
  else lua_pushinteger(L, (uint32_t)(c - VRAM_MMAP[address.Base_Offset].Index));
  return 1;
}
/// compare({address_1},{address_2}, Length) 
static int VRAM_memcmp(lua_State *L){
  uint8_t index = 1;
  _VRAM_t address_1 = VRAM_HELPER_get_address(L, &index);
  _VRAM_t address_2 = VRAM_HELPER_get_address(L, &index);
  uint16_t length = luaL_optinteger(L, index++, 1);
  if (length == 0) return 0;
  if (!VRAM_HELPER_Check_Limit(&address_1, length)) { luaL_error(L, "Attempt to access outside memory range"); return 0; }
  if (!VRAM_HELPER_Check_Limit(&address_2, length)) { luaL_error(L, "Attempt to access outside memory range"); return 0; }
  lua_pushinteger(L, memcmp(address_1.ptr,address_2.ptr,length)); 
  return 1;
}
/// copy({address_to},{address_from}, Length) 
static int VRAM_memcpy(lua_State *L){
  uint8_t index = 1;
  _VRAM_t address_1 = VRAM_HELPER_get_address(L, &index);
  _VRAM_t address_2 = VRAM_HELPER_get_address(L, &index);
  uint16_t length = luaL_optinteger(L, index++, 1);
  if (length == 0) return 0;
  if (!VRAM_MMAP[address_1.Base_Offset].R_W)
  {
    luaL_error(L,"ERROR: The memory at \"%s\" is read-only", VRAM_OFFSETS[address_1.Base_Offset]);
    lua_pushboolean(L, false);
    return 1;
  }
  memcpy(address_1.ptr,address_2.ptr,length); 
  return 0;
}
/// move({address_to},{address_from}, Length) 
static int VRAM_memmove(lua_State *L){
  uint8_t index = 1;
  _VRAM_t address_1 = VRAM_HELPER_get_address(L, &index);
  _VRAM_t address_2 = VRAM_HELPER_get_address(L, &index);
  uint16_t length = luaL_optinteger(L, index++, 1);
  if (length == 0) return 0;
  if (!VRAM_MMAP[address_1.Base_Offset].R_W)
  {
    luaL_error(L,"ERROR: The memory at \"%s\" is read-only", VRAM_OFFSETS[address_1.Base_Offset]);
    lua_pushboolean(L, false);
    return 1;
  }
  memmove(address_1.ptr,address_2.ptr,length); 
  return 0;
}
/// set({address}, Data, Length, [numBytes])
static int VRAM_memset(lua_State *L){
  uint8_t index = 1;
  _VRAM_t address = VRAM_HELPER_get_address(L, &index);
  uint32_t data = luaL_optinteger(L, index++, 1);
  uint16_t length = luaL_optinteger(L, index++, 1);
  uint8_t numBytes = luaL_optinteger(L, index++, 1);
  if (!VRAM_HELPER_is_aligned(address.ptr,numBytes))
  {
    luaL_error(L, "Attempt to access unaligned memory");
    return 0;
  }
  if (!VRAM_MMAP[address.Base_Offset].R_W)
  {
    luaL_error(L,"ERROR: The memory at \"%s\" is read-only", VRAM_OFFSETS[address.Base_Offset]);
    lua_pushboolean(L, false);
    return 1;
  }
  switch (numBytes ) { 
    case 1:
      memset((uint8_t *)address.ptr, data, length); 
      break;
    case 2:
      memset2((uint16_t *)address.ptr, SWAP_UINT16(data), length);
      break;
    case 4:
      memset4((uint32_t*)address.ptr, SWAP_UINT32(data), length);
      break;
    default:
      luaL_error(L,"Invalid byte length %d",numBytes);
  }
  return 0;
}
static int VRAM_xor8(lua_State *L){
  uint8_t  p = 0;
  uint16_t i = 0;
  uint8_t  l = 1;
  switch (lua_type(L,1))
  {
    case LUA_TNUMBER:
        i = luaL_optinteger(L, 1, 0);
        l = luaL_optinteger(L, 2, 1);
        break;
    case LUA_TSTRING:
        p = luaL_checkoption(L, 1, VRAM_OFFSETS[0], VRAM_OFFSETS);
        i = luaL_optinteger(L, 2, 0);
        l = luaL_optinteger(L, 3, 1);
        break;
    default:
        luaL_error(L, "Invalid index type");
  }
  if (l == 0) return 0;
  lua_pushinteger(L, *(VRAM_MMAP[p].Base + VRAM_MMAP[p].Index + i));
  *(VRAM_MMAP[p].Base + VRAM_MMAP[p].Index + i) ^= l;
  return 1;
}
static int VRAM_or8(lua_State *L){
  uint8_t  p = 0;
  uint16_t i = 0;
  uint8_t  l = 1;
  switch (lua_type(L,1))
  {
    case LUA_TNUMBER:
        i = luaL_optinteger(L, 1, 0);
        l = luaL_optinteger(L, 2, 1);
        break;
    case LUA_TSTRING:
        p = luaL_checkoption(L, 1, VRAM_OFFSETS[0], VRAM_OFFSETS);
        i = luaL_optinteger(L, 2, 0);
        l = luaL_optinteger(L, 3, 1);
        break;
    default:
        luaL_error(L, "Invalid index type");
  }
  if (l == 0) return 0;
  lua_pushinteger(L, *(VRAM_MMAP[p].Base + VRAM_MMAP[p].Index + i));
  *(VRAM_MMAP[p].Base + VRAM_MMAP[p].Index + i) |= l;
  return 1;
}
static int VRAM_and8(lua_State *L){
  uint8_t  p = 0;
  uint16_t i = 0;
  uint8_t  l = 1;
  switch (lua_type(L,1))
  {
    case LUA_TNUMBER:
        i = luaL_optinteger(L, 1, 0);
        l = luaL_optinteger(L, 2, 1);
        break;
    case LUA_TSTRING:
        p = luaL_checkoption(L, 1, VRAM_OFFSETS[0], VRAM_OFFSETS);
        i = luaL_optinteger(L, 2, 0);
        l = luaL_optinteger(L, 3, 1);
        break;
    default:
        luaL_error(L, "Invalid index type");
  }
  if (l == 0) return 0;
  lua_pushinteger(L, *(VRAM_MMAP[p].Base + VRAM_MMAP[p].Index + i));
  *(VRAM_MMAP[p].Base + VRAM_MMAP[p].Index + i) &= l;
  return 1;
}
// vram.load({to_address}, filename, [length]) 
static int VRAM_load(lua_State *L)
{
  uint16_t bufsize;
  uint8_t index = 1;
  _VRAM_t address = VRAM_HELPER_get_address(L, &index);
  if (!VRAM_MMAP[address.Base_Offset].R_W)
  {
    luaL_error(L,"ERROR: The memory at \"%s\" is read-only", VRAM_OFFSETS[address.Base_Offset]);
    lua_pushboolean(L, false);
    return 1;
  }
  const char* filename = luaL_optstring(L,index++,"mem.dump");
  uint16_t length = luaL_optinteger(L, index++, 0);
    FIL fpd;
    FIL *fp = &fpd;
    if (f_open(fp, filename, FA_READ) != FR_OK) { goto FILE_ERR; }
    if (fseek((FILE*)fp, 0, SEEK_END) == 0) {
      bufsize = ftell((FILE*)fp);
      if (bufsize == -1) { goto FILE_ERR; }
      if (fseek((FILE*)fp, 0, SEEK_SET) != 0) { goto FILE_ERR; }
      if (length == 0) length = VRAM_MMAP[address.Base_Offset].Length;
      fread(address.ptr, sizeof(char), length > bufsize ? bufsize : length, (FILE*)fp);
      if ( f_error( fp ) != 0 ) { goto FILE_ERR; } 
    }
    f_close(fp);
    lua_pushboolean(L, true);
    lua_pushinteger(L, f_error( fp ));
    return 2;

FILE_ERR:
  lua_pushboolean(L, false);
  lua_pushinteger(L, f_error( fp ));
  f_close(fp);
  return 2;
}
// vram.save({from_address}, length, filename)
static int VRAM_save(lua_State *L)
{
  uint8_t index = 1;
  _VRAM_t address = VRAM_HELPER_get_address(L, &index);
  uint16_t length = luaL_optinteger(L, index++, 1);
  const char* filename = luaL_optstring(L,index++,"mem.dump");
    FIL fpd;
    FIL *fp = &fpd;
    if (f_open(fp, filename, FA_WRITE + FA_CREATE_NEW) != FR_OK) { goto FILE_ERR; }
    fwrite(address.ptr, sizeof(char), length, (FILE*)fp);
    if ( f_error( fp ) != 0 ) { goto FILE_ERR; } 
    f_close(fp);
    lua_pushboolean(L, true);
    lua_pushinteger(L, f_error( fp ));
    return 2;

FILE_ERR:
  lua_pushboolean(L, false);
  lua_pushinteger(L, f_error( fp ));
  f_close(fp);
  return 2;
}
#ifdef VRAM_TT
static int VRAM_table_test(lua_State *L)
{
    luaL_checktype(L, 1, LUA_TTABLE);
    // let alone excessive arguments (idiomatic), or do:
    lua_settop(L, 1);

    int a_size = lua_rawlen(L, 1); // absolute indexing for arguments

    puts("<C Function START>");
    printf("LENGTH of DATA  %d\n\r",a_size);
    for (int i = 1; i <= a_size; i++) {
        lua_pushinteger(L, i);
        lua_gettable(L, 1); 

        if (lua_isnil(L, -1)) { // relative indexing for "locals"
            a_size = i-1; // fix actual size (e.g. 4th nil means a_size==3)
            break;
        }

        if (!lua_isnumber(L, -1)) // optional check
            return luaL_error(L, "item %d invalid (number required, got %s)",
                              i, luaL_typename(L, -1));

        lua_Integer b = lua_tointeger(L, -1);

        if (b < 0 || b > UINT8_MAX) // optional
            return luaL_error(L, "item %d out of range", i);

        printf("%2d:%02X ",i, b & 0xFF);

        lua_pop(L, 1);
    }
    
    Enter();
    puts("<C Function DONE>");

    return 0;
}
#endif

static const luaL_Reg vramlib[] = {
  {"read",    VRAM_read_mem8},
  {"write",   VRAM_write_mem8}, 
  {"search",  VRAM_memchr}, 
  {"compare", VRAM_memcmp}, 
  {"copy",    VRAM_memcpy}, 
  {"move",    VRAM_memmove},
  {"set",     VRAM_memset}, 
  {"load",    VRAM_load}, 
  {"save",    VRAM_save}, 
  {"bit_flip",VRAM_xor8},
  {"bit_clr", VRAM_and8},
  {"bit_set", VRAM_or8},
#ifdef VRAM_TT
  {"wt",    VRAM_table_test},
#endif
  {NULL, NULL}
};

/* }====================================================== */

LUAMOD_API int luaopen_vram (lua_State *L) {
    luaL_newlib(L, vramlib);
    return 1;
}