/*
    This is the PT52 Library it give access to most PT52 functions
*/

#define lptlib_c
#define LUA_LIB

#include "pt52io.h"
#include <signal.h>
#include <string.h>

#include "lua.h"

#include "lauxlib.h"
#include "lualib.h"
#if 0 // example
static int os_execute (lua_State *L) {
  const char *cmd = luaL_optstring(L, 1, NULL);
  int stat;
  errno = 0;
  stat = system(cmd);
  if (cmd != NULL)
    return luaL_execresult(L, stat);
  else {
    lua_pushboolean(L, stat);  /* true if there is a shell */
    return 1;
  }
}
#endif

/** ALARM CODE **/
#define ALARM_NAME	"alarm handler"

static lua_State *alarmL=NULL;
static lua_Hook alarm_hook=NULL;
static int alarm_mask=0;
static int alarm_count=0;

static int64_t alarm_Callback (alarm_id_t id, void *user_data)
{
    raise(SIGALRM);
    return 0; // do not repeat
}

static void alarm_handler(lua_State *L, lua_Debug *ar)
{
    L=alarmL;
    lua_sethook(L, alarm_hook, alarm_mask, alarm_count);
    lua_pushliteral(L, ALARM_NAME);
    lua_gettable(L, LUA_REGISTRYINDEX);
    lua_call(L,0,0);
}

static void alarm_signal(int i)
{
    signal(i,SIG_DFL);
    alarm_hook = lua_gethook(alarmL);
    alarm_mask = lua_gethookmask(alarmL);
    alarm_count = lua_gethookcount(alarmL);
    lua_sethook(alarmL, alarm_handler, LUA_MASKCALL | LUA_MASKRET | LUA_MASKCOUNT, 1);
}

static int l_alarm(lua_State *L) 
{
    alarmL=L;
    switch (lua_gettop(L))
    {
        case 0:
            break;
        case 1:
            lua_pushliteral(L, ALARM_NAME);
            lua_gettable(L, LUA_REGISTRYINDEX);
            if (lua_isnil(L, -1)) luaL_error(L,"Alarm handler is not set");
            break;
        default:
            luaL_checktype(L, 2, LUA_TFUNCTION);
            lua_pushliteral(L, ALARM_NAME);
            lua_pushvalue(L, 2);
            lua_settable(L, LUA_REGISTRYINDEX);
    }
    if (signal(SIGALRM, alarm_signal)==SIG_ERR) { lua_pushnil(L); }
    else { lua_pushinteger(L,add_alarm_in_ms(luaL_optinteger(L,1,0) * 100, alarm_Callback, NULL, true)); }
    return 1;
}
/** END ALARM CODE **/
/** KEYBOARD CODE **/
#define KEY_NAME	"keyboard handler"

static lua_State *keyL=NULL;
static lua_Hook key_hook=NULL;
static int key_mask=0;
static int key_count=0;

static void key_handler(lua_State *L, lua_Debug *ar)
{
    L=keyL;
    lua_sethook(L,key_hook,key_mask,key_count);
    lua_pushliteral(L,KEY_NAME);
    lua_gettable(L,LUA_REGISTRYINDEX);
    lua_call(L,0,0);
}

static void key_signal(int i)
{
    signal(i,SIG_DFL);
    key_hook=lua_gethook(keyL);
    key_mask=lua_gethookmask(keyL);
    key_count=lua_gethookcount(keyL);
    lua_sethook(keyL,key_handler,LUA_MASKCALL | LUA_MASKRET | LUA_MASKCOUNT,1);
}

static int set_key_handler(lua_State *L)
{
    keyL=L;
    if (lua_gettop(L) == 0)
    {
        lua_pushliteral(L,KEY_NAME);
        lua_gettable(L,LUA_REGISTRYINDEX);
        if (lua_isnil(L,-1)) luaL_error(L,"no keyboard handler set");
    } else {
        luaL_checktype(L,1,LUA_TFUNCTION);
        lua_pushliteral(L,KEY_NAME);
        lua_pushvalue(L,1);
        lua_settable(L,LUA_REGISTRYINDEX);
    }
    if (signal(SIGIO,key_signal)==SIG_ERR) { lua_pushnil(L); }
    else { lua_pushboolean(L,true); }
    return 1;
}
static int clr_key_handler(lua_State *L)
{
    if (signal(SIGIO,SIG_DFL)==SIG_ERR) { lua_pushboolean(L,false); }
    else { lua_pushboolean(L,true); }
    return 1;
}
/** END KEYBOARD CODE **/

static int pt_Set_Textmode(lua_State *L) {
    uint16_t m = luaL_optinteger(L, 1, 0);
    switch (m){
        case 0: lua_pushinteger(L, _Display_Mode + 1); return 1;
        case 1: _Display_Mode = MODE_TXT; Reset_Character_Buffer(); break;
        case 2: _Display_Mode = MODE_T2;  Reset_Character_Buffer(); break;
        default: luaL_error(L,"OUT OF BOUNDS");
    }
    return 0;
}

static int pt_rst_scn(lua_State *L) {
    Reset_Character_Buffer();
    return 0;
}

static int pt_clear_screen(lua_State *L) {
    uint8_t c = luaL_optinteger(L, 1, 0);
    Clear_screen(c);
    return 0;
}

static int pt_clear_line(lua_State *L) {
    uint8_t c = luaL_optinteger(L, 1, 0);
    Clear_line(c);
    return 0;
}

static int pt_cursorto(lua_State *L) {
    uint8_t c = luaL_optinteger(L, 1, 0);
    uint8_t l = luaL_optinteger(L, 2, 0);
    Move_cursorto(c,l);
    return 0;
}

static int pt_put_char_free(lua_State *L) {
    uint8_t c = luaL_optinteger(L, 1, 0);
    uint8_t l = luaL_optinteger(L, 2, 0);
    const char *chr = luaL_optstring(L, 3, NULL);
    if (chr == NULL) return 1;
    Put_charp(c,l, chr[0]);
    return 0;
}
static int pt_put_char(lua_State *L) {
    char chr = 0;
    if (lua_isnil(L, 1))
    {
        return 0;
    }
    if (lua_isinteger(L, 1))
    {
        chr = (uint8_t)luaL_optinteger(L, 1, 0);
    } else if(lua_isstring(L,1))
    {
        chr = luaL_optstring(L, 1, NULL)[0];
    }
    Put_char(chr);
    Cursor_Position_Check();
    return 0;
}
static int pt_print(lua_State *L) {
    const char* s = luaL_optstring(L, 1, NULL);
    for (; *s != 0; *s++) Decode_Character(*s);
}
static int pt_save_cursor(lua_State *L) {
    Save_cursor();
    return 0;
}

static int pt_restore_cursor(lua_State *L) {
    Restore_cursor();
    return 0;
}
static int pt_Set_BColour(lua_State *L) {
    uint8_t c = luaL_optinteger(L, 1, 0);
    Set_BColour(c);
    return 0;
}

static int pt_Set_FColour(lua_State *L) {
    uint8_t c = luaL_optinteger(L, 1, 0);
    Set_FColour(c);
    return 0;
}

static int pt_Set_Colours(lua_State *L) {
    uint8_t c = luaL_optinteger(L, 1, 0);
    Set_Colours(c);
    return 0;
}

static int pt_Set_Pallet(lua_State *L) {
    uint8_t c = luaL_optinteger(L, 1, 0);
    Set_Pallet(c);
    return 0;
}

static int pt_Set_Attrib_Extended(lua_State *L) {
    uint8_t c = luaL_optinteger(L, 1, 0);
    Set_Attrib_Extended(c ? 1 : 0);
    return 0;
}

static int pt_Set_HEADER(lua_State *L) {
    const char *text = luaL_optstring(L, 1, NULL);
    if (text != NULL) {
        _VBUFFERS.VIDEO_REG.HEAD_ENABLE = 1;
        Set_HEADER(text);
    } else {
        CLR_HEADER();
        _VBUFFERS.VIDEO_REG.HEAD_ENABLE = 0;
    }
    return 0;
}

static int pt_Set_FOOTER(lua_State *L) {
    const char *text = luaL_optstring(L, 1, NULL);
    if (text != NULL) {
        _VBUFFERS.VIDEO_REG.FOOT_ENABLE = 1;
        Set_FOOTER(text);
    } else {
        CLR_FOOTER();
        _VBUFFERS.VIDEO_REG.FOOT_ENABLE = 0;
    }
    return 0;
}

static int pt_Use_FontSet(lua_State *L) {
    uint8_t c = luaL_optinteger(L, 1, 0) % 4;
    Use_FontSet(c);
    return 0;
}
uint8_t FNTBUFF_load_bin(const char *filename);
static int pt_Load_Font(lua_State *L) {
    if (lua_isinteger(L, 1)) {
        uint8_t i = (int)luaL_optinteger(L, 1, 0) & 3;
        // memcpy(_VBUFFERS.FONT_BUFFER,&fontdata_8x8[i * 0x100 * GLYPH_HEIGHT], 0x100 * GLYPH_HEIGHT);
        memcpy(_VBUFFERS.FONT_BUFFER,&font_rom[i * 0x100 * GLYPH_HEIGHT], 0x100 * GLYPH_HEIGHT);
    }
    const char *filename = luaL_optstring(L, 1, NULL);
    FNTBUFF_load_bin(filename);
    return 0;
}
/* static int pt_define_char(lua_State *L) {
    lua_settop(L,0); //This ensures that our array is placed on the top of the current stack.
    lua_getglobal(L, “array”);
    //If lua_settop(L,0) is not called before, then we must use luaL_len(L,-1)
    int n = luaL_len(L, 1); //luaL_len can get the number of elements in the table
    for (int i = 1; i <= n; ++i) {
        lua_pushnumber(L, i); //Push i into the stack
        lua_gettable(L, -2); //Read table[i], table is at -2 position.
        //lua_rawget(L, -2); //lua_gettable can also be replaced by lua_rawget
        cout<<lua_tostring(L, -1)<<endl;
        lua_pop(L, 1);
    }
} */
char Getchar();
keycode_t get_lastkey();
static int pt_Getchar(lua_State *L) {
    lua_pushinteger(L, Getchar());
    return 1;
}
static int pt_get_lastkeycode(lua_State *L) {
    keycode_t k = get_lastkey();
    lua_pushinteger(L, k.keycode);
    return 1;
}
static int pt_sleep(lua_State *L) {
    uint32_t t = luaL_optinteger(L, 1, 0); 
    sleep_ms(t);
    return 0;
}
keycode_t get_last_keycode();
static int pt_get_last_keycode(lua_State *L) {
    keycode_t r = get_last_keycode();
    lua_pushinteger(L, r.keycode);
    lua_pushinteger(L, r.character);
    lua_pushinteger(L, r.Flags);
    return 3;
}
void print_mem_list();
static int pt_memlist(lua_State *L) {
    print_mem_list();
    return 0;
}

static const luaL_Reg ptlib[] = {
    {"resetscreen",     pt_rst_scn},
    {"clr",             pt_clear_screen},
    {"clearline",       pt_clear_line},
    {"cursorto",        pt_cursorto},
    {"putchar_free",    pt_put_char_free},
    {"putchar",         pt_put_char},
    {"print",           pt_print},
    {"cursor_save",     pt_save_cursor},
    {"cursor_restore",  pt_restore_cursor},
    {"set_bg",          pt_Set_BColour},
    {"set_fg",          pt_Set_FColour},
    {"set_colours",     pt_Set_Colours},
    {"set_pallet",      pt_Set_Pallet},
    {"set_exteneded",   pt_Set_Attrib_Extended},
    {"set_header",      pt_Set_HEADER},
    {"set_footer",      pt_Set_FOOTER},
    {"set_textmode",    pt_Set_Textmode},
    {"use_fontset",     pt_Use_FontSet},
    {"load_font",       pt_Load_Font},
    {"getchar",         pt_Getchar},
    {"get_keycode",     pt_get_lastkeycode},
    {"sleep",           pt_sleep},
    {"memlist",         pt_memlist},
    {"alarm",           l_alarm},
    {"key_report",      pt_get_last_keycode},
    {"set_key_handle",  set_key_handler},
    {"clr_key_handle",  clr_key_handler},
    {NULL, NULL}
};

/* }====================================================== */



LUAMOD_API int luaopen_pt (lua_State *L) {
    luaL_newlib(L, ptlib);
    return 1;
}
