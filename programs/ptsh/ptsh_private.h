#ifndef _PTSH_PRIVATE_H
#define _PTSH_PRIVATE_H

#include <stdlib.h>
#include "ptsh_cmds.h"

#include "ptsh_conf.h"

#define MK_CMD(x) int ptsh_ ## x (int argc,char **args)
#define MK_APP(x) int app_ ## x (int argc,char **args)
#define xstr(s) #s
#define N_CMD(x) xstr(x) 
#define F_CMD(x) &ptsh_ ## x 
#define F_APP(x) &app_## x

/*
  Function Declarations for builtin shell commands:
 */
MK_CMD(help);
MK_CMD(exit);

struct ptsh_func {
    int (*_func) (int,char **);
    char *_name;
};

struct ptsh_func function_list[] = {
    {&ptsh_help,"help"},
    {&ptsh_exit,"exit"},
    PTSH_CMDS_LIST,
#ifdef APP_EDIT
    {APP_EDIT,      "edit"},
#endif
#ifdef APP_LUA
    {APP_LUA,        "lua"},
#endif
#ifdef APP_LUAC
    {APP_LUAC,        "luac"},
#endif
#ifdef APP_RUN
    {APP_RUN,        "run"},
#endif
#ifdef APP_HEXEDIT
    {APP_HEXEDIT,    "hexedit"},
#endif
#ifdef APP_TERMINAL
    {APP_TERMINAL,   "terminal"},
#endif
    {NULL,              NULL},
};

#endif