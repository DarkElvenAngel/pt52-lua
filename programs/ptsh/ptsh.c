// PT52 Shell


#include <sys/wait.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
//#include "pt52dc.h"
#include "pt52io.h"
#include "ptsh.h"

#include "ptsh_private.h"

jmp_buf ptsh_buffer;
char **ptsh_split_line(char *line, int *count);

#define PTSH_MAXLENGTH_FILENAME 127
char exitstr[5];
ptsh_env_t ptsh_env = {
  .exitcode = 0,
  .lastcommand = ""
};
#define PTSH_PATH ptsh_env.path
/**
 * @brief Get variable from the environment list
 * 
 * @param name 
 * @return char* pointer to the value in the
       environment, or NULL if there is no match.
 *
 * @details
 * function searches the environment list to find the
       environment variable name, and returns a pointer to the
       corresponding value string.
 */
char *getenv(const char *name)
{
  if (strcmp(name,"?") == 0){
    snprintf(exitstr,4,"%d", ptsh_env.exitcode);
    return exitstr;
  }
  if (strcmp(name,"!") == 0){
    return ptsh_env.lastcommand;
  }
  for (int i = 0; i < PTSH_ENV_VARS; i++) {
    if (ptsh_env.env_vars[i][0] != '\0' && strncmp(ptsh_env.env_vars[i], name, PTSH_ENV_NAME -1) == 0) {
      return ptsh_env.env_vars[i] + PTSH_ENV_NAME + 1; // Return the corresponding value
    }
  }
  return NULL; // Not found
}

/**
 * @brief Set variable from the environment list
 *
 * @param name 
 * @param value 
 * @param overwrite 
 * @return int  zero on success, or -1 on error
 * 
 * @details
 * function adds the variable name to the environment
       with the value value, if name does not already exist.  If name
       does exist in the environment, then its value is changed to value
       if overwrite is nonzero; if overwrite is zero, then the value of
       name is not changed (and setenv() returns a success status).
       This function makes copies of the strings pointed to by name and
       value
 */
int setenv(const char *name, const char *value, int overwrite)
{
  for (int i = 0; i < PTSH_ENV_VARS; i++) {
    if (ptsh_env.env_vars[i][0] != '\0' && strncmp(ptsh_env.env_vars[i], name, PTSH_ENV_NAME -1) == 0) {
      // If variable exists
      if (overwrite) {
        memset(ptsh_env.env_vars[i] + PTSH_ENV_NAME + 1, 0, PTSH_ENV_VALUE);
        strncpy(ptsh_env.env_vars[i] + PTSH_ENV_NAME + 1, value, PTSH_ENV_VALUE - 1);
        return 0; // Success
      }
      return 0; // No overwrite, but success
    }
    if (ptsh_env.env_vars[i][0] == '\0') {
      // Found an empty slot to set the new variable
      memset(ptsh_env.env_vars[i], 0, PTSH_ENV_KV_PAIR_SIZE);
      strncpy(ptsh_env.env_vars[i], name, PTSH_ENV_NAME - 1);
      strncpy(ptsh_env.env_vars[i] + PTSH_ENV_NAME + 1, value, PTSH_ENV_VALUE - 1);
      return 0; // Success
    }
  }
  return -1; // Environment full
}
/**
 * @brief deletes variable from the environment.
 * 
 * @param name 
 * @return int  zero on success, or -1 on error
 * 
 * @details
 * If name does not exist in the environment, then the
       function succeeds, and the environment is unchanged.
 */
int unsetenv(const char *name)
{
  for (int i = 0; i < PTSH_ENV_VARS; i++) {
    if (ptsh_env.env_vars[i][0] != '\0' && strncmp(ptsh_env.env_vars[i], name, PTSH_ENV_NAME -1) == 0) {
      memset(ptsh_env.env_vars[i], 0, PTSH_ENV_KV_PAIR_SIZE);
      return 0; // Success
    }
  }
  return 0;
}

void PTSH_EXIT(int e)
{
#ifdef PTSH_PRINT_EXIT_CODE
  printf ("EXIT code [%d]\n\r",e);
#endif
  longjmp(ptsh_buffer, 1);
}

int ptsh_num_builtins() {
  return (sizeof(function_list) / sizeof(struct ptsh_func)) - 1;
}

int ptsh_help(int argc,char **args)
{
  int i;
  puts("PT52 SHELL");
  printf("version %s\n\r", PTSH_VERSION);
  puts("Type program names and arguments, and hit enter.");
  puts("The following are built in:");

  for (i = 0; i < ptsh_num_builtins(); i++) {
    printf("%-40s%-40s\r", function_list[i]._name, (i + 1) < ptsh_num_builtins() ? function_list[++i]._name : "");
  }
  return 0;
}

int ptsh_exit(int argc,char **args)
{
  return 1;
}

#if 1
FRESULT ptsh_find_file(char *filename, FILINFO* fno) {
  if (f_stat(filename, fno) == FR_OK) {
    if (!(fno->fattrib & AM_DIR)) return FR_OK; // verify this is a file and not a directory
  }
  // Get the PATH environment variable
  char *path_env = getenv("PATH");
  if (path_env == NULL) return FR_NO_FILE;

  // Directory buffer for constructing new paths
  char chkname[PTSH_MAXLENGTH_FILENAME + 1] = {0};
  const char *start = path_env;
  const char *end;
    
  do {
    end = strchr(start, ';');
    size_t length = (end != NULL) ? (end - start) : strlen(start);
    snprintf(chkname, sizeof(chkname), "%.*s/%s", (int)length, start, filename);
    // Check if the constructed path exists
    if (f_stat(chkname, fno) == FR_OK) {
      if (fno->fattrib & AM_DIR) continue; // verify this is a file and not a directory
        strncpy(filename, chkname, PTSH_MAXLENGTH_FILENAME);
        filename[PTSH_MAXLENGTH_FILENAME] = '\0'; // Ensure null termination
        return FR_OK;  // File found
    }
    start = end ? end + 1 : end;
  } while (end != NULL);
  return FR_NO_FILE;  // File not found in any path
}
#else
FRESULT ptsh_find_file(char * filename, FILINFO* fno) {
  char *found = strrchr(filename, '/');
  if (f_stat(filename,fno)==FR_OK) return FR_OK;
  if (!found) {
    char chkname[PTSH_MAXLENGTH_FILENAME + 1] = {0};
    int pathIdx = 0;
    if (ptsh_env.path_count == 0) return FR_NO_FILE;
    do  {
      strncpy(chkname, PTSH_PATH[pathIdx],PTSH_MAXLENGTH_FILENAME);
      uint8_t len = strlen(chkname);
      if (chkname[len - 1 ] != '/') {
        chkname[len++] = '/';
      }
      strncat(chkname,filename,PTSH_MAXLENGTH_FILENAME);
      if (f_stat(chkname,fno)==FR_OK) { strncpy(filename,chkname,PTSH_MAXLENGTH_FILENAME); return FR_OK;}
    } while (++pathIdx <= ptsh_env.path_count);
  }
  return FR_NO_FILE;
}
#endif

int ptsh_launch(int argc,char **argv)
{
  FRESULT fr;
  FILINFO fno;
  char Filename[PTSH_MAXLENGTH_FILENAME + 1];
  Filename[PTSH_MAXLENGTH_FILENAME] = '\0';
  if (argv[0][0] == '!') strncpy(Filename, &argv[0][1],PTSH_MAXLENGTH_FILENAME);
  else strncpy(Filename, argv[0],PTSH_MAXLENGTH_FILENAME);
  fr = ptsh_find_file(Filename, &fno);
  if (fr == FR_OK && (fno.fattrib & AM_SYS || argv[0][0] == '!') )
  {
    char *savep = argv[0];
    argv[0]=Filename;
    pt_reset_memory();
    int ret = (*(APP_LUA))(argc,argv);
    pt_reset_memory();
    argv[0]=savep;
    return ret;
  }
  printf("Bad Command or Filename: %s\n\r",argv[0]);
  return 1;
}

int ptsh_call_autoexec()
{
  FRESULT fr;
  FILINFO fno;
  char *argv[1] = {"/autoexec"};
  fr = ptsh_find_file(argv[0], &fno);
  if (fr == FR_OK && (fno.fattrib & AM_SYS) )
  {
    pt_reset_memory();
    int ret = (*(APP_LUA))(1,argv);
    pt_reset_memory();
    return ret;
  }
  return 0;
}

int ptsh_execute(int argc, char **args)
{
  int i;

  if (args[0] == NULL) {
    // An empty command was entered.
    return 0;
  }

  pt_reset_memory();
  for (i = 0; i < ptsh_num_builtins(); i++) {
    if (strcmp(args[0], function_list[i]._name) == 0) {
      return (*function_list[i]._func)(argc,args);
    }
  }

  return ptsh_launch(argc,args);
}

char* ptsh_strtok(char* str, const char* NOT_USED) {
    // Static variables to keep track of the input string and position
    static char* current_str = NULL;
    static char* current_pos = NULL;
    char seek = ' ';
    if (str == NULL) {
        if (current_str == NULL) return NULL; // Return NULL if str and current_str are NULL
        current_str = current_pos ;
    } else {
        current_pos = current_str = str;
        //current_pos = str;
    }

    if (*current_pos != '\0') {
        while (*current_str == ' ') { *current_str++; current_pos++; }
        if (*current_pos == '"') { seek = '"'; current_pos++; current_str++; }
        current_pos = strchr(current_pos, seek);

        if (current_pos != NULL) {
            *current_pos = '\0';    // Null-terminate the current token
            current_pos++; // Move to the next character after the delimiter
        } else {
            // No more delimiters, update the current position to point to the end of the string
            current_pos = strchr(current_str, 0);
        }
    } else {
        return NULL;
    }
    return current_str;
}
#define PTSH_TOK_BUFSIZE 64
#define PTSH_TOK_DELIM " \t\r\n\a"

char **ptsh_split_line(char *line, int *count) {
  static char* _PTSH_tokens[PTSH_TOK_BUFSIZE];
  int bufsize = PTSH_TOK_BUFSIZE, position = 0;
  char *token;

  token = ptsh_strtok(line, PTSH_TOK_DELIM);
  while (token != NULL) {
    _PTSH_tokens[position] = token;
    position++;

    if (position >= bufsize) {
      puts("ptsh: buffer overflow");
      *count = -1;
      return NULL;
    }

    token = ptsh_strtok(NULL, PTSH_TOK_DELIM);
  }
  _PTSH_tokens[position] = NULL;
  *count = position;
  return _PTSH_tokens;
}
extern char buf_printf[PRINTF_BUFFER_SIZE];
int ptsh_var_replace(char* string)
{
  char *Token = strchr(string, '$');
  if (!Token) return 0;
  char *Token_start = Token ++;
  char target[20] = {0};
  do  {
    Token++;
    if (*Token < 48 || *Token > 126) { // Check if the character is valid within the range
      int len = Token - Token_start - 1;
      strncpy(target, Token_start + 1, len < 19 ? len : 19);
      strncpy(buf_printf,string, Token_start - string);
      char *replacement = getenv(target);
      if (replacement == NULL) {
        replacement = "";
      }
      strncpy(buf_printf + (Token_start - string), replacement, strlen(replacement));
      strncpy(buf_printf + (Token_start - string) + strlen(replacement), Token, strlen(string));
      strncpy(string, buf_printf, 255);
      string[255] = '\0';
      memset(buf_printf,0,PRINTF_BUFFER_SIZE);
      if (Token_start) Token = Token_start + 1;
      return 1;
    }
  } while (*Token && Token_start);
  return 0;
}

void ptsh_loop(void)
{
  char *line;
  char **args;
  int status = 0;
  // int ret = 0;
  int argc = 0;
  ptsh_env.exitcode = ptsh_call_autoexec();
  do {
    Set_Cursor(true);
    char prompt[26];
    f_getcwd(prompt, 23);
    strncat(prompt," > ", 25);
    line = readline(prompt);
    while (ptsh_var_replace(line));
    memset(ptsh_env.lastcommand,0,127);
    strncpy(ptsh_env.lastcommand,line,126);
    args = ptsh_split_line(line, &argc);
    if (args != NULL) {
      if ((ptsh_env.exitcode  = setjmp(ptsh_buffer)) == 0) 
        ptsh_env.exitcode = ptsh_execute(argc, args);
    } else {
      puts("SYSTEM ERROR!");
      return;
    }
    readline_free(line);
  } while (status == 0);
}