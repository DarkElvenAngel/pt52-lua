#ifndef _PTSH_CMDS_H_
#define _PTSH_CMDS_H_

int ptsh_attrib(int argc, char **argv);
int ptsh_cls (int argc, char** argv);
int ptsh_mem (int argc, char** argv);
int ptsh_ls (int argc, char** argv);
int ptsh_type (int argc, char** argv);
int ptsh_ver (int argc, char** argv);
int ptsh_cd(int argc, char** argv);
int ptsh_mkdir(int argc, char** argv);
int ptsh_move(int argc, char** argv);
int ptsh_copy(int argc, char **argv);
int ptsh_del(int argc, char **argv);
int ptsh_reboot(int argc, char** argv);
int ptsh_set(int argc, char** argv);
int ptsh_unset(int argc, char** argv);

#define PTSH_CMDS_LIST {&ptsh_attrib,"attrib"},\
                       {&ptsh_cls,"cls"},\
                       {&ptsh_mem,"mem"},\
                       {&ptsh_ls,"ls"},\
                       {&ptsh_type,"type"},\
                       {&ptsh_ver,"ver"},\
                       {&ptsh_cd,"cd"},\
                       {&ptsh_mkdir,"mkdir"},\
                       {&ptsh_move,"move"},\
                       {&ptsh_copy,"copy"},\
                       {&ptsh_del,"del"},\
                       {&ptsh_reboot,"reboot"},\
                       {&ptsh_set,"set"},\
                       {&ptsh_unset,"unset"}

#endif
