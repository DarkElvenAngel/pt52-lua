#ifndef PTSH_H
#define PTSH_H
#define PTSH_VERSION "0.0.6"

#include <setjmp.h>

extern jmp_buf ptsh_buffer;
void PTSH_EXIT(int);
void ptsh_loop(void);

#undef exit
#define exit(e) PTSH_EXIT(e)

#define PTSH_ENV_VARS 10         // Maximum number of environment variables
#define PTSH_ENV_NAME 21         // Maximum length for variable names + 1 for the null terminator
#define PTSH_ENV_VALUE 101       // Maximum length for variable values + 1 for the null terminator
#define PTSH_ENV_KV_PAIR_SIZE   PTSH_ENV_NAME + PTSH_ENV_VALUE

typedef struct {
    int8_t exitcode;                                    // Last exit code
    // char path[MAX_PATH_ENTRIES][MAX_PATH_LENGTH];    // Static array for shell paths
    // int path_count;                                  // Actual count of paths in use
    char lastcommand[127];
    // Environment variables management
    char env_vars[PTSH_ENV_VARS][PTSH_ENV_KV_PAIR_SIZE];
} ptsh_env_t;
 #define ptevs sizeof(ptsh_env_t)
extern ptsh_env_t ptsh_env;
char *getenv(const char *name);
int setenv(const char *name, const char *value, int overwrite);
int unsetenv(const char *name);
#endif