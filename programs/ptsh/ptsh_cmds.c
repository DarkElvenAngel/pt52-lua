#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "hardware/watchdog.h"
#include "pico/bootrom.h"

#include "pt52io.h"
#include "ptsh.h"

static const char *FRESULT_str(FRESULT i) {
    switch (i) {
        case FR_OK:
            return "Succeeded";
        case FR_DISK_ERR:
            return "A hard error occurred in the low level disk I/O layer";
        case FR_INT_ERR:
            return "Assertion failed";
        case FR_NOT_READY:
            return "The physical drive cannot work";
        case FR_NO_FILE:
            return "Could not find the file";
        case FR_NO_PATH:
            return "Could not find the path";
        case FR_INVALID_NAME:
            return "The path name format is invalid";
        case FR_DENIED:
            return "Access denied due to prohibited access or directory full";
        case FR_EXIST:
            return "Access denied due to prohibited access (exists)";
        case FR_INVALID_OBJECT:
            return "The file/directory object is invalid";
        case FR_WRITE_PROTECTED:
            return "The physical drive is write protected";
        case FR_INVALID_DRIVE:
            return "The logical drive number is invalid";
        case FR_NOT_ENABLED:
            return "The volume has no work area (mount)";
        case FR_NO_FILESYSTEM:
            return "There is no valid FAT volume";
        case FR_MKFS_ABORTED:
            return "The f_mkfs() aborted due to any problem";
        case FR_TIMEOUT:
            return "Could not get a grant to access the volume within defined "
                   "period";
        case FR_LOCKED:
            return "The operation is rejected according to the file sharing "
                   "policy";
        case FR_NOT_ENOUGH_CORE:
            return "LFN working buffer could not be allocated";
        case FR_TOO_MANY_OPEN_FILES:
            return "Number of open files > FF_FS_LOCK";
        case FR_INVALID_PARAMETER:
            return "Given parameter is invalid";
        default:
            return "Unknown";
    }
}
static int ptsh_argp(int argc, char **argv, char *options[], bool *opt, uint8_t count){
    for(int k=1;k<argc;k++){
        bool f = false;
        for(int i=0;i<count;i++)
            if(strcmp(options[i],argv[k])==0)
                f = opt[i] = true;
        if(!f) return k;
    }
    return argc;
}

int ptsh_cls (int argc, char** argv)
{
    Home(0);
    Clear_screen(0);
    return 0;
}
uint32_t getTotalHeap(void);
uint32_t getFreeHeap(void);
int ptsh_mem (int argc, char** argv)
{
    /* char* free_memory = NULL;
    uint8_t x;
    for (x = 1; x < 250; x++)
    {
        free_memory = malloc(1024 * x);
        if (free_memory == NULL)
        {
            break;
        }
        free(free_memory);
        free_memory = NULL;
    } */

    printf ("System Reports Approximately %d bytes free\n\r", getFreeHeap());
    return 0;
}
int ptsh_ls (int argc, char** argv)
{
    char cwdbuf[FF_LFN_BUF] = {0};
    FRESULT fr; /* Return value */
    char const *p_dir;
    if (argc == 1)
    {
        fr = f_getcwd(cwdbuf, sizeof cwdbuf);
        if (FR_OK != fr) {
            puts("No such file or directory");
            return 1;
        }
        p_dir = cwdbuf;
    } else {
        fr = f_stat(argv[1], NULL);
        if (FR_OK != fr) {
            puts("No such file or directory");
            return 1;
        }
        p_dir = argv[1];
    }
    printf("Directory of 0:%s\n\r", p_dir);
    DIR dj;      /* Directory object */
    FILINFO fno; /* File information */
    memset(&dj, 0, sizeof dj);
    memset(&fno, 0, sizeof fno);
    fr = f_findfirst(&dj, &fno, p_dir, "*");
    if (FR_OK != fr) {
        return 1;
    }
    uint32_t t_size = 0; uint16_t t_files = 0, t_dirs = 0;
    while (fr == FR_OK && fno.fname[0]) { /* Repeat while an item is found */
        /* Create a string that includes the file name, the file size and the
         attributes string. */
        const char *pcWritableFile = "writable file",
                   *pcReadOnlyFile = "read only file",
                   *pcDirectory = "directory";
        const char *pcAttrib;
        uint8_t fileColour= 047;
        /* Point pcAttrib to a string that describes the file. */
        if (fno.fattrib & AM_DIR) {
            pcAttrib = pcDirectory;
            fileColour = 054;
            t_dirs++;
        } else if (fno.fattrib & AM_RDO) {
            pcAttrib = pcReadOnlyFile;
            fileColour = 041;
            t_files++;
        } else {
            pcAttrib = pcWritableFile;
            fileColour = 052;
            t_files++;
        }
        /* Create a string that includes the file name, the file size and the
         attributes string. */
        printf("%c%c%c%c%c \ef%c%-15s\ed %10u byte(s)\n\r"
            , fno.fattrib & AM_DIR ? 'd' : '.'		/* Directory */
            , fno.fattrib & AM_RDO ? 'r' : 'w'		/* Read only */
            , fno.fattrib & AM_HID ? 'h' : '.'		/* Hidden */
            , fno.fattrib & AM_SYS ? 'x' : '.'		/* System */
            , fno.fattrib & AM_ARC ? 'a' : '.'		/* Archive */
            , fileColour
            , fno.fname
            , fno.fsize
        );
        t_size += fno.fsize;
        fr = f_findnext(&dj, &fno); /* Search for next item */
    }
    printf("%5d File(s)   %10u bytes\n\r%5d Dir(s)\n\r", t_files, t_size, t_dirs);
    f_closedir(&dj);
    return 0;
}
int ptsh_type (int argc, char** argv)
{
    FIL fil;
    FRESULT fr = f_open(&fil, argv[1], FA_READ);
    if (FR_OK != fr) {
        printf("\ef041\eR error: %s Not found or not Accessible\er\ed\n\r", argv[1]);
        return 1;
    }
    putchar('\e');putchar('N');
    char buf[256] = { 0 };
    while (f_gets(buf, sizeof buf - 1, &fil)) {
        printf("%s", buf);
    }
    fr = f_close(&fil);
    putchar('\e');putchar('n');
    if (FR_OK != fr) { printf("\ef041\eR error: %s (%d)\er\ed\n\r", argv[1]);return 1;}
    return 0;
}
int ptsh_ver (int argc, char** argv)
{
    printf ("ptsh version %s\n\r", PTSH_VERSION);
    return 0;
}
int ptsh_cd(int argc, char** argv) {
    FRESULT fr;
    if (argc != 2) {
    char str[80];
        fr = f_getcwd(str, 80);
        if (FR_OK != fr) return 1;
        puts(str);
        return 0;
    }
    fr = f_chdir(argv[1]);
    if (FR_OK != fr) 
    {
        puts("invalid directory");
        return 1;
    }
    return 0;
}
int ptsh_mkdir(int argc, char** argv) {
    if (argc != 2) {
        printf("mkdir: missing operand\n\r");
        return 1;
    }
    FRESULT fr = f_mkdir(argv[1]);
    if (FR_OK != fr) {
        printf("mkdir: error %s (%d)\n\r", FRESULT_str(fr), fr);
        return 1;
    }
    return 0;
}
int ptsh_move(int argc, char** argv) {
    if (argc != 3) {
        puts("mv: missing file operand");
        return 1;
    }
    FRESULT fr = f_rename (argv[1], argv[2]);
    if (FR_OK != fr) {
        printf("move: error %s (%d)\n\r", FRESULT_str(fr), fr);
        return 1;
    }
    return 0;
}
int ptsh_copy(int argc, char **argv)
{
    FILE *fp1;
    FILE *fp2;

    if (argc != 3)
        puts("copy: missing file operand");
    if ((fp1 = fopen(argv[1], "rb")) == 0) {
        printf("cannot open file %s for reading\n\r", argv[1]);
        return 1;
    }
    if ((fp2 = fopen(argv[2], "wb")) == 0) {
        printf("cannot open file %s for writing\n\r", argv[2]);
        fclose(fp1);
        return 1;
    }
    char            buffer[512];
    size_t          n;

    while ((n = fread(buffer, sizeof(char), sizeof(buffer), fp1)) > 0)
    {
        if (fwrite(buffer, sizeof(char), n, fp2) != n)
            printf("write failed\n\r");
    }
    fclose(fp1);
    fclose(fp2);

    return 0;
}
int ptsh_del(int argc, char **argv)
{
    if (argc != 2){
        puts("rm: missing operand");
        return 1;
    }
    FRESULT fr = f_unlink (argv[1]);
    if (FR_OK != fr){
        printf("rm: error %s (%d)\n\r", FRESULT_str(fr), fr);
        return 1;
    }
    return 0;
}
int ptsh_attrib(int argc, char **argv)
{
    FRESULT fr;
    FILINFO fno;
    if (argc == 1) {
        puts("attrib: missing operand");
        return 1;
    }
    const char *fname = argv[1];

    fr = f_stat(fname, &fno);
    switch (fr) {

    case FR_OK:
        if (argc == 2){
        printf("Size: %lu\n\r", fno.fsize);
        printf("Timestamp: %u-%02u-%02u, %02u:%02u\n\r",
               (fno.fdate >> 9) + 1980, fno.fdate >> 5 & 15, fno.fdate & 31,
               fno.ftime >> 11, fno.ftime >> 5 & 63);
        printf("Attributes: %c%c%c%c%c\n\r",
               (fno.fattrib & AM_DIR) ? 'D' : '-',
               (fno.fattrib & AM_RDO) ? 'R' : '-',
               (fno.fattrib & AM_HID) ? 'H' : '-',
               (fno.fattrib & AM_SYS) ? 'X' : '-',
               (fno.fattrib & AM_ARC) ? 'A' : '-');
        puts("NOTE: System Attribute is mapped the \eSExecute\es bit");
        }
        break;

    case FR_NO_FILE:
        puts("File not found");
        return 1;

    default:
        printf("An error occurred. (%d)\n", fr);
        return 1;
    }
    if (argc == 2 || fno.fattrib & AM_DIR) return 0;
    char *p = argv[2];
    if (*p != '+' && *p != '-') {
        puts("Attrib string much start with + or -");
        return 1;
    }
    char attrib_mode = *p;
    uint8_t set_attrib = 0, clr_attrib = 0;
    p++;
    while (*p)
    {
        uint8_t bit_attrib = 0;
        switch (tolower(*p))
        {
            case 'r': bit_attrib = AM_RDO; break;
            case 'h': bit_attrib = AM_HID; break;
            case 's': bit_attrib = AM_SYS; break;
            case 'x': bit_attrib = AM_SYS; break;
            case 'a': bit_attrib = AM_ARC; break;
            default: puts("ERROR in value"); return 1;
        }
        if (attrib_mode == '+') set_attrib |= bit_attrib; else clr_attrib |= bit_attrib;
        p++;
    }
    // printf("DATA SET %02x CLR %02x\n\r" ,set_attrib , clr_attrib);
    /* Set Read-only, clear Archive and others are left unchanged. */
    // f_chmod("file.txt", AM_RDO, AM_RDO | AM_ARC);
    if (attrib_mode == '+')
    {
        puts(FRESULT_str(f_chmod(fname, set_attrib, set_attrib)));
        return 0;
    }
    puts(FRESULT_str(f_chmod(fname, 0, clr_attrib)));
    return 0;
}

int ptsh_reboot(int argc, char** argv)
{
    bool act = false;
    if (argc > 1)
    {
        if (argv[1][0] == '-')
            switch (argv[1][1])
            {
            case 'u':
                act = true;
                break;
            case 'h':
            default:
                printf ("USAGE: reboot [-u]\n\n\r reboot device\n\r  -u in BOOTSEL mode\n\r");
                return 0;
            }
    }
    sleep_ms(500);
    if (act) reset_usb_boot(0, 0);
    else watchdog_reboot(0, 0, 1);
    return 0;
}

int ptsh_set(int argc, char **argv)
{
    if (argc != 2) {
        for (uint8_t i = 0; i < PTSH_ENV_VARS; i++)
        {
            if (ptsh_env.env_vars[i][0]){
                printf("%s=%s\n\r", ptsh_env.env_vars[i], ptsh_env.env_vars[i] + PTSH_ENV_NAME + 1);
            }
        }
        return 0;
    }

    char *value = strchr(argv[1], '=');
    if (value == NULL) {
        printf("Invalid format\n\rUsage: %s VAR=value\n\r", argv[0]);
        return -1; // Error code
    }
    *(value++) = '\0';

    return setenv(argv[1], value, 1);
}

int ptsh_unset(int argc, char **argv)
{
    if (argc < 2) {
        printf("Usage: %s VAR\n\r", argv[0]);
        return -1;
    }
    return unsetenv(argv[1]);
}
