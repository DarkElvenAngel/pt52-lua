# Before you raise an issue

Please remember to be respectful and polite, check if your issue has been resolved in recent commits.

If this is a feature request I should warn you that I'm not ready to add features at this time.
