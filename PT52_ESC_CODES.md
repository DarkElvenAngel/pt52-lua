# PT52 Escape Codes

**[ESC A]** - Move_cursor Up  
**[ESC B]** - Move_cursor Down  
**[ESC C]** - Move_cursor Right  
**[ESC D]** - Move_cursor Left  
**[ESC T]** - Fill screen with 'E'  
**[ESC F]** - Graphics Mode  
**[ESC G]** - Text Mode  
**[ESC g _Graphical Set#_]** - Load Graphics Set _n_  
**[ESC H]** - Home  
**[ESC I]** - Reverse_Linefeed  
**[ESC J]** - Clear to end of screen  
**[ESC j]** - Clear screen  
**[ESC K]** - Clear to end of line  
**[ESC k]** - Clear line  
**[ESC Y _line#_ _column#_]** - Move to line column  
**[ESC Z]** - Report Terminal Type  
**[ESC 5]** - Show Cursor  
**[ESC 6]** - Hide Cursor  
**[ESC 7]** - Save Cursor  
**[ESC 8]** - Restore Cursor  
**[ESC =]** - Alternate Keypad mode on  
**[ESC >]** - Alternate Keypad mode off  
**[ESC b _colour#_]** - Set Background Colour  
**[ESC f _colour#_]** - Set Foreground Colour  
**[ESC p _pallet#_]** - Set Colour Pallet  
**[ESC c]** - Clear Attributes  
**[ESC d]** - Restore Default Colours  
**[ESC S]** - Bold on  
**[ESC s]** - Bold off  
**[ESC R]** - Reverse on  
**[ESC r]** - Reverse off  
**[ESC U]** - Underline on  
**[ESC u]** - Underline off  
**[ESC N]** - Newline with linefeed  
**[ESC n]** - Newline only  
**[ESC _ _char_ _data x 16_]** - Define a custom character

**ALL CODES** with more than one letter have _italicized_ parameters these parameters are numbers. however they are passed in the same way the VT52 For example "_line#_", the host sends the octal code 040 to specify line 0 of the screen, and octal code 067 to select the 23 of the display.  
In the case of "_colour#_" it's not possible to access the full pallet of 256 colours if you use the single character method, so you can use it's hex value "00" - "ff" The decoder accepts 0-9 a-f A-F. The value **MUST** be two characters.

## Escape CSI (Control Sequence Introducer)

"**ESC [**" is the ANSI escape sequence for CSI this sequence is used in several programs as standard output, this would normally throw garbage all over the terminal as ANSI CSI codes are not handled on the VT52, however on the PT52 we have a solution all CSI codes end in a known character so the PT52 can identify a CSI code and ignore it.

## Graphics Mode

The PT52 has a pseudo graphical mode just like the VT52 if in graphical mode the normal. This mode _banks_ out the lower case letters and a few symbols and replaces them with the loaded graphical set.  There are 33 characters in a set and there are currently 4 sets: Blox, VT52, VT100, and Boxes. Only one set can be shown on screen at any time, and all 33 characters user definable!

## User Definable Characters

The graphics mode characters are all user definable if a none of the presets is suitable it's easy to enter new ones.  A character to be defined must be between 0x53 '^' and 0x7E '~'.  The character is generated MSB from the top down, each row is 2 nibbles and there are 8 rows for 16 nibbles total.

The method of entering a nibble is using it's hex value \[0-F\]  this is not case sensitive.  If an invalid character is chosen to be replaced the escape handler will exit the sequence after the invalid character is received, such that the 16 following nibbles will be printed to the display.
