#ifndef ATTRIBUTES_H
#define ATTRIBUTES_H
#include "config.h"

#pragma pack(1)
struct Char_Attrib_s
{
    union
    {
        struct {
            uint8_t underline   : 1;
            uint8_t reverse     : 1;
            uint8_t extended    : 1;
#ifdef ENABLE_VT100
            uint8_t intensity   : 1;
#endif
/*             uint8_t bold        : 1;
            uint8_t codepage437 : 1;
            uint8_t fontBuffer  : 1; */

        /* Select what font to render with.
         * 0 - Normal
         * 1 - BOLD
         * 2 - Code Page 437
         * 3 - Font Buffer
         */
            uint8_t Use_Font    : 2; 
        };
        uint8_t Flags;
    };

    union
    {
        struct {
            uint8_t foreground_colour;
            uint8_t background_colour;
        };
        uint16_t colours;
    };
    
    uint8_t character;
};// Attributes;

extern struct Char_Attrib_s Attributes;
/**
 * Set background colour
 * 
 * \param colour value 0 - 15
 * \return none
 */
void Set_BColour(uint16_t colour);

/**
 * Set foreground colour
 * 
 * \param colour value 0 - 15
 * \return none
 */
void Set_FColour(uint16_t colour);

/**
 * Set both colours
 * 
 * \param colour value 0 - 15
 * \return none
 */
void Set_Colours(uint32_t colours);

/**
 * Set pallet
 * 
 * \param index value 0 - 4
 * \return none
 */
void Set_Pallet(uint8_t index);

/**
 * Set Atrribute Underline
 * 
 * \param value boolean
 * \return none
 */
void Set_Attrib_Underline(bool value);
/**
 * Set Attribute Reverse
 * 
 * \param value boolean
 * \return none
 */
void Set_Attrib_Reverse(bool value);
/**
 * Set Attribute Extended
 * 
 * \param value boolean
 * \return none
 */
void Set_Attrib_Extended(bool value);
/**
 * Set Attribute Bold
 * 
 * \param value boolean
 * \return none
 */
void Set_Attrib_Bold(bool value);
/**
 * Set Attribute Code Page 437 font
 * 
 * \param value boolean
 * \return none
 */
void Set_Attrib_codepage437(bool value);
/**
 * Set Attribute Uses Font Buffer
 * 
 * \param value boolean
 * \return none
 */
void Set_Attrib_fontBuffer(bool value);
/**
 * Set Font
 * 
 * \param value boolean
 * \return none
 */
void Set_Attrib_Font(uint8_t value);

#ifdef ENABLE_VT100
/**
 * Set Attribute Intensity
 * 
 * \param value boolean
 * \return none
 */
void Set_Attrib_Intensity(bool value);
#endif
/**
 * Set Attribute Flags
 * 
 * \param value Flag
 * \return none
 */
void Set_Attrib_Flags(uint8_t value);
#endif