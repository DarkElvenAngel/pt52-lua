#ifndef _DRAW_H
#define _DRAW_H
#include <stdint.h>

typedef void (*Pixel_Plotter)(uint16_t x, uint16_t y, uint8_t c);

void draw_line(Pixel_Plotter PP, int x0, int y0, int x1, int y1, int c);
void draw_rect(Pixel_Plotter PP, int x0, int y0, int w, int h, int c);
void fill_rect(Pixel_Plotter PP, int x0, int y0, int w, int h, int c);
void draw_circle(Pixel_Plotter PP, int x0, int y0, int r, int c);
void fill_circle(Pixel_Plotter PP, int x0, int y0, int r, int c);
void draw_ellipse(Pixel_Plotter PP, int xc,int yc,int rx,int ry, int c);
void draw_polygon(Pixel_Plotter PP, int num_vertices,int *vertices, int c) ;
void drawCircleHelper(Pixel_Plotter PP, int x0, int y0, int r, int corner_name, int color);
void drawRoundRect(Pixel_Plotter PP, int x, int y, int w, int h, int r, int c);
#endif