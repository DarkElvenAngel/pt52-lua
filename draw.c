
/* ---------------------------------------------------------------------
 * PRIMITIVE Drawing
 * These function are mostly complete they will need the colour by index
 * to be removed and updated.
 * 
 */
#include "draw.h"

void draw_line(Pixel_Plotter PP, int x0, int y0, int x1, int y1, int c) {
    int dx = x1 - x0;
    dx = (dx >= 0) ? dx : -dx; // abs()
    int dy = y1 - y0;
    dy = (dy >= 0) ? dy : -dy; // abs()
    int sx;
    int sy;
    if (x0 < x1)
        sx = 1;
    else
        sx = -1;
    if (y0 < y1)
        sy = 1;
    else
        sy = -1;
    int err = dx - dy;
    int e2;
    int done = 0;
    while (!done) {
        PP(x0, y0, c);
        if ((x0 == x1) && (y0 == y1))
            done = 1;
        else {
            e2 = 2 * err;
            if (e2 > -dy) {
                err = err - dy;
                x0 = x0 + sx;
            }
            if (e2 < dx) {
                err = err + dx;
                y0 = y0 + sy;
            }
        }
    }
}

// helper function to draw a rectangle outline in given color
void draw_rect(Pixel_Plotter PP, int x0, int y0, int w, int h, int c) {
    draw_line(PP, x0, y0, x0 + w, y0, c); // top
    draw_line(PP, x0, y0, x0, y0 + h, c); // left
    draw_line(PP, x0, y0 + h, x0 + w, y0 + h, c); // bottom
    draw_line(PP, x0 + w, y0, x0 + w, y0 + h, c); // right
}

// helper function to draw a rectangle outline in given color
void fill_rect(Pixel_Plotter PP, int x0, int y0, int w, int h, int c) {
    int y;
    for (y = 0; y < h; y++) {
        draw_line(PP, x0, y0 + y, x0 + w, y0 + y, c);
    }
}

// helper function to draw a circle outline in given color
// (uses Bresenham's circle algorithm)
void draw_circle(Pixel_Plotter PP, int x0, int y0, int r, int c){
    int x = r;
    int y = 0;
    int radiusError = 1 - x;

    while(x >= y)
    {
        // top left
        PP(-y + x0, -x + y0, c);
        // top right
        PP(y + x0, -x + y0, c);
        // upper middle left
        PP(-x + x0, -y + y0, c);
        // upper middle right
        PP(x + x0, -y + y0, c);
        // lower middle left
        PP(-x + x0, y + y0, c);
        // lower middle right
        PP(x + x0, y + y0, c);
        // bottom left
        PP(-y + x0, x + y0, c);
        // bottom right
        PP(y + x0, x + y0, c);

        y++;
        if (radiusError < 0)
        {
            radiusError += 2 * y + 1;
        } else {
            x--;
            radiusError+= 2 * (y - x + 1);
        }
    }
}

// helper function to draw a filled circle in given color
// (uses Bresenham's circle algorithm)
void fill_circle(Pixel_Plotter PP, int x0, int y0, int r, int c) {
    int x = r;
    int y = 0;
    int radiusError = 1 - x;

    while(x >= y)
    {
        // top
        draw_line(PP, -y + x0, -x + y0, y + x0, -x + y0, c);
        // upper middle
        draw_line(PP, -x + x0, -y + y0, x + x0, -y + y0, c);
        // lower middle
        draw_line(PP, -x + x0, y + y0, x + x0, y + y0, c);
        // bottom 
        draw_line(PP, -y + x0, x + y0, y + x0, x + y0, c);

        y++;
        if (radiusError < 0)
        {
            radiusError += 2 * y + 1;
        } else {
            x--;
            radiusError+= 2 * (y - x + 1);
        }
    }
}
void draw_ellipse(Pixel_Plotter PP, int xc,int yc,int rx,int ry, int c) {
    int x, y, p;
    x=0;
    y=ry;
    p=(ry*ry)-(rx*rx*ry)+((rx*rx)/4);
    while((2*x*ry*ry)<(2*y*rx*rx))
    {
        PP(xc+x,yc-y,c);
        PP(xc-x,yc+y,c);
        PP(xc+x,yc+y,c);
        PP(xc-x,yc-y,c);

        if(p<0)
        {
            x=x+1;
            p=p+(2*ry*ry*x)+(ry*ry);
        } else {
            x=x+1;
            y=y-1;
            p=p+(2*ry*ry*x+ry*ry)-(2*rx*rx*y);
        }
    }
    p=((float)x+0.5)*((float)x+0.5)*ry*ry+(y-1)*(y-1)*rx*rx-rx*rx*ry*ry;

    while(y>=0)
    {
        PP(xc+x,yc-y,c);
        PP(xc-x,yc+y,c);
        PP(xc+x,yc+y,c);
        PP(xc-x,yc-y,c);

        if(p>0)
        {
            y=y-1;
            p=p-(2*rx*rx*y)+(rx*rx);
        } else {
            y=y-1;
            x=x+1;
            p=p+(2*ry*ry*x)-(2*rx*rx*y)-(rx*rx);
        }
    }
}
void draw_polygon(Pixel_Plotter PP, int num_vertices,int *vertices, int c) {
  int i;
  for(i=0;i<num_vertices-1;i++)
  {
    draw_line(PP, vertices[(i<<1)+0],
         vertices[(i<<1)+1],
         vertices[(i<<1)+2],
         vertices[(i<<1)+3],
         c);
  }
  draw_line(PP, vertices[0],
       vertices[1],
       vertices[(num_vertices<<1)-2],
       vertices[(num_vertices<<1)-1],
       c);
}
void drawCircleHelper(Pixel_Plotter PP, int x0, int y0, int r, int corner_name, int color) {
    int f     = 1 - r;
    int ddF_x = 1;
    int ddF_y = -2 * r;
    int x     = 0;
    int y     = r;

    while (x<y) {
        if (f >= 0) {
            y--;
            ddF_y += 2;
            f     += ddF_y;
        }
        x++;
        ddF_x += 2;
        f     += ddF_x;
        if (corner_name & 0x4) {
            PP(x0 + x, y0 + y, color);
            PP(x0 + y, y0 + x, color);
        }
        if (corner_name & 0x2) {
            PP(x0 + x, y0 - y, color);
            PP(x0 + y, y0 - x, color);
        }
        if (corner_name & 0x8) {
            PP(x0 - y, y0 + x, color);
            PP(x0 - x, y0 + y, color);
        }
        if (corner_name & 0x1) {
            PP(x0 - y, y0 - x, color);
            PP(x0 - x, y0 - y, color);
        }
    }
}
void drawRoundRect(Pixel_Plotter PP, int x, int y, int w, int h, int r, int c) {
    int max_radius = ((w < h) ? w : h) / 2; // 1/2 minor axis
    if(r > max_radius) r = max_radius;
    
    draw_line(PP, x + r, y    , x + w - r, y        , c);
    draw_line(PP, x + r, y + h, x + w - r, y + h    , c);
    draw_line(PP, x    , y + r, x        , y + h - r, c);
    draw_line(PP, x + w, y + r, x + w    , y + h - r, c);
    
    // draw four corners
    drawCircleHelper(PP, x+r  , y+r  , r, 1, c);
    drawCircleHelper(PP, x+w-r, y+r  , r, 2, c);
    drawCircleHelper(PP, x+w-r, y+h-r, r, 4, c);
    drawCircleHelper(PP, x+r  , y+h-r, r, 8, c);
    
}