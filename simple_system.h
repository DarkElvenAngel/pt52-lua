#ifndef SIMPLE_SYSTEM_H
#define SIMPLE_SYSTEM_H

#undef getchar
#define getchar Getchar
#undef scanf
#define scanf Scanf

/**
 * Read formatted input from keyboard.
 * 
 * \param format controls the output as in C printf.
 * \return the number of input items successfully matched and assigned
 */
int Scanf(const char *format, ...);


/**
 * Get Line from the user.  /!\ IMPORTANT free the returned pointer!
 * 
 * \param const char* prompt what prompt you want to use can be NULL
 * \return char* returns the text of the line read. A blank line returns the empty string.
 */
char *readline(const char* prompt);

/**
 * Get characater 
 * 
 * \return char returns the last character pressed.
 */
char Getchar();

/**
 * Get Last Keypress
 * 
 * \return keycode_t
 */
keycode_t get_lastkey();

/**
 * Key_Pressed Callback for Keyboard function
 * 
 * \param Ctrl - control key
 * \param Shift - shift key
 * \param Alt - alt key
 * \param keycode that needs processing
 * \return void
 */
void Key_Pressed(bool Ctrl, bool Shift, bool Alt,uint8_t keycode);
#endif