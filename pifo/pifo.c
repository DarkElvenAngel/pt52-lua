
#include "pifo.h"


void    pifo_init(pifo_t* p)
{
    p->read_pos = 0;
    p->write_pos = 0;
}
int16_t pifo_write(pifo_t* p, uint8_t data)
{
    if (pifo_status(p) == PIFO_FULL) return PIFO_FULL;
    p->data[p->write_pos++] = data;
    uint8_t a = p->write_pos - p->read_pos;
    return a;
}
int16_t pifo_read(pifo_t* p)
{
    if (pifo_status(p) == PIFO_EMPTY) return PIFO_EMPTY;
    return p->data[p->read_pos++];
}
int16_t pifo_status(pifo_t* p)
{
    uint8_t a = p->write_pos - p->read_pos; 
    switch (a)
    {
        case 0: return PIFO_EMPTY;
        case PIFO_SIZE - 1: return PIFO_FULL;
        default: return a;
    }
}
void pifo_flush(pifo_t* p)
{
    p->write_pos = p->read_pos;
}
char*   pifo_strerr(int16_t err)
{
    switch(err)
    {
        case 0: return "PIFO EMPTY";
        case PIFO_FULL: return "PIFO FULL";
        case PIFO_SIZE - 1: return "PIFO AT CAPACITY";
        default: return "OK";
    }
}