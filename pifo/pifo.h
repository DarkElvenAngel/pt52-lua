#ifndef _PIFO_H
#define _PIFO_H

#include <stdint.h>

#ifndef PIFO_SIZE
#define PIFO_SIZE 256
#elif PIFO_SIZE > 256
#error "PIFO Buffer size exceeds max size [256]"
#endif

#define PIFO_EMPTY  -1
#define PIFO_FULL   -2


typedef struct 
{
    char    data[PIFO_SIZE];
    uint8_t read_pos;
    uint8_t write_pos;
} pifo_t;


void    pifo_init(pifo_t*);
int16_t pifo_write(pifo_t*, uint8_t);
int16_t pifo_read(pifo_t*);
int16_t pifo_status(pifo_t*);
void pifo_flush(pifo_t* p);
char*   pifo_strerr(int16_t);
#endif