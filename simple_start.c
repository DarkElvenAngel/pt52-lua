
#include "pico.h"
#include "pico/stdlib.h"
#include <stdlib.h>
#include <ctype.h>
#include <stdarg.h>
#include <signal.h>
#include "pt52io.h"
#include "hardware/clocks.h"

#include "bsp/board.h"
#include "tusb.h"
#include "usb-keyboard.h"
#ifdef FEMTO_PICO_LIB
#include "femto/src/LineEditor.h"
#endif

/* Include the Lua API header files. */
#include "programs/lua/lua.h"
#include "programs/lua/lauxlib.h"
#include "programs/lua/lualib.h"
#include <malloc.h>

/* char* readline(const char* p){
    return NULL;
} */

uint32_t getTotalHeap(void) {
   extern char __StackLimit, __bss_end__;
   
   return &__StackLimit  - &__bss_end__;
}

uint32_t getFreeHeap(void) {
   struct mallinfo m = mallinfo();

   return getTotalHeap() - m.uordblks;
}
//static char last_char = 0;
//static keycode_t last_keycode = { 0 };
/**
 * Read formatted input from keyboard.
 * 
 * \param format controls the output as in C printf.
 * \return the number of input items successfully matched and assigned
 */
int Scanf(const char *format, ...)
{
    char* s = readline(NULL);
    va_list args;
    va_start(args, format);
    int r = vsscanf(s, format, args);
    va_end(args);
    readline_free(s);
    return r;
}
/**
 * Get Line from the user.  /!\ IMPORTANT free the returned pointer!
 * 
 * \param const char* prompt what prompt you want to use can be NULL
 * \return char* returns the text of the line read. A blank line returns the empty string.
 */
#ifndef FEMTO_PICO_LIB
char *readline(const char* prompt)
{
    // This is a blocking call
    if (prompt != NULL) printf(prompt);     // print the prompt if one is given
    char* ret = (char*)malloc(160 * sizeof(char));   // allocate memory for the return data
    if (ret == NULL)
    {
        printf("\r\n\eSERROR!  Unable to allocate memory for readline!\es\r\n");
        return NULL;
    }
    memset(ret,0,160 *sizeof(char));
    last_char = 0;                          // reset the last_char so we don't read old data
        uint8_t c = 0;                      // keep track of the number of characters are entered
    while (last_char != '\r')               // note that last_char is updated in the Key_pressed function
    {
        if (last_char >= ' ')               // new data is available and is not the end line.
        {
            ret[c++] = last_char;           // add new character to the return increment the character counter
            putchar(last_char);
            last_char = 0;                  // reset last_char
        }
        if (last_char == 0x08 && c > 0 )    // Backspace
        {
            Backspace();
            c--;                            // decrement c 
            last_char = 0;                  // reset last_char 
        }
        if (last_char == 0x04)              // CTRL [D]
        {
            free(ret);
            return NULL;
        }
        if (c >= 160 - 2) break;                 // if the buffer is almost full exit loop early.
        sleep_ms(1);
    }
}
#endif

/**
 * Get character 
 * 
 * \return char returns the last character pressed.
 */
char Getchar()
{
    last_char = 0;
    while (last_char == 0)
    {
        sleep_ms(1);
    }
    return last_char;
}

/**
 * Get Last Keypress
 * 
 * \return keycode_t
 */
keycode_t get_lastkey()
{
    last_keycode = (keycode_t){ 0 };
    while (last_keycode.keycode == 0)
    {
        sleep_ms(1);
    }
    return last_keycode;
}

keycode_t get_last_keycode() { return last_keycode; }
//  This is the replacement for simple_terminal and vga_terminal
uint8_t const keycode2ascii[128][3] = {HID_KEYCODE_TO_ASCII_WITH_CTRL};
/**
 * Key Pressed Event
 * 
 * \param Ctrl is the control key pressed
 * \param Shift is the shift key pressed
 * \param keycode 
 * \return none 
 */
void Key_Pressed(bool Ctrl, bool Shift, bool Alt, uint8_t keycode)
{
    bool LockKey_Caps = keylock_flags & KEYBOARD_LED_CAPSLOCK;
    bool LockKey_Num = keylock_flags & KEYBOARD_LED_NUMLOCK;
    if (LockKey_Caps && ! Ctrl) if (keycode >= 0x04 && keycode <= 0x1D ) Shift -= 1;
    if (LockKey_Num && ! Ctrl) if (keycode >= 0x59 && keycode <= 0x63 ) Shift -= 1;
    static bool SYS_MENU = false;
    uint8_t ch = keycode2ascii[keycode][(Shift && !Ctrl) ? 1 : 0 + Ctrl ? 2 : 0];
    last_keycode.shift = Shift;
    last_keycode.ctrl = Ctrl;
    last_keycode.alt = Alt;
    last_keycode.character = ch;
    last_keycode.keycode = keycode;
    switch (ch)
    {
        case 0:
        if (keycode == 0x46 && !SYS_MENU) {                             // SysRq 
            Set_Display_Mode(MODE_BLK);
            Set_HEADER("PT52-Lua [ SYSTEM MENU ]");
            Set_FOOTER("");
            Overlay_clear(0);
            _VBUFFERS.VIDEO_REG.STAT_ENABLE = 0;
            _VBUFFERS.VIDEO_REG.HEAD_ENABLE = 1;
            _VBUFFERS.VIDEO_REG.HEAD_Pallet = 1;
            _VBUFFERS.VIDEO_REG.HEADER_BG = 8;
            _VBUFFERS.VIDEO_REG.HEADER_FG = 0;
            _VBUFFERS.VIDEO_REG.FOOT_ENABLE = 1;
            _VBUFFERS.VIDEO_REG.FOOT_Pallet = 1;
            _VBUFFERS.VIDEO_REG.FOOTER_BG = 8;
            _VBUFFERS.VIDEO_REG.FOOTER_FG = 0;
            _VBUFFERS.VIDEO_REG.OVERLAY_ENABLE = 1;
            SYS_MENU = 1; 
            break;
        }
        if(_VBUFFERS.VIDEO_REG._KEYBOARD_FN_KEYS == 1) break;
        switch (keycode) // non-printable characters
        {
            //case 0x1F: if (Ctrl) uart_putc_raw(UART_ID, 0);break;   // CTRL @ send NULL
            case 0x40: Set_Display_Mode(MODE_TXT); break;                  // F8  key mode txt
            case 0x41: Set_Display_Mode(MODE_T2); break;                  // F8  key mode txt
            case 0x42: Set_Display_Mode(MODE_G0); break;                  // F9  key mode g0
            case 0x43: Set_Display_Mode(MODE_G1); break;                  // F10 key mode g1
            case 0x44: Set_Display_Mode(MODE_G2); break;                  // F11 key mode g2
            case 0x45: Set_Display_Mode(MODE_G3); break;                  // F12 key mode g3
            //case 0x45: Local_Tools -= 1;                            // F12 key [LOCAL ECHO]
            /*case 0x46: Reset_Character_Buffer(); break;             // PRINT SCREEN
            case 0x48: if (Ctrl) uart_puts(UART_ID, "PT52\n");
            case 0x4c: uart_putc_raw(UART_ID, 127);break;           // Delete key
            case 0x4f: uart_puts(UART_ID, "\eC"); break;            // cursorX += cursorX <= COLS-1 ? 1 : 0; break;// right
            case 0x50: uart_puts(UART_ID, "\eD"); break;            // cursorX -= cursorX > 0 ? 1 : 0; break;// left
            case 0x51: uart_puts(UART_ID, "\eB"); break;            // cursorY += cursorY <= FOOTER - 2 ? 1 : 0; break;// down
            case 0x52: uart_puts(UART_ID, "\eA"); break;            // cursorY -= cursorY > HEADER ? 1 : 0; break;//up
            */
            //case 0x65: Setup_Menu(); break;                       // Menu Key
            default   :  //Decode_Character(ch); break;//uart_putc_raw(UART_ID, ch);break;
            if (SYS_MENU)
            {
                
            }
        } 
        break; 
        case 0x03: if(_VBUFFERS.VIDEO_REG._KEYBOARD_CTRL_C == 0) raise(SIGINT);
        //case 0x08 : uart_putc_raw(UART_ID, ch);break;             // BackSpace 
        //case 0x09 : uart_putc_raw(UART_ID, ch);break;             // Tab Key
        //case 0x1b : uart_putc_raw(UART_ID, ch);break;             // Escape key
        //case '\r' : uart_puts(UART_ID, "\n\r");break;             // ENTER []
        default   :  //Decode_Character(ch); break;//uart_putc_raw(UART_ID, ch);break;
            if (SYS_MENU)
            {
                Overlay_clear(0);
                Overlay_print(2,2,4,"PRESS A KEY");
                Overlay_print(2,12,4,"KEYCODE [0x%02X]\n\r\t"
                    "\x1b~%d[CTRL]\x1b~G \x1b~%d[SHIFT]\x1b~G %02X",
                    keycode, 
                    Ctrl ? 4 : 2, 
                    Shift ? 4 : 2, 
                    ch
                );
            } else {
                // Decode_Character(ch);//uart_putc_raw(UART_ID, ch);
                // if (ch > ' ') Cmd_String[Cmd_Index++] = ch;
                last_char = ch;
            }
            break;
   }
   raise(SIGIO);
}
bool Keyboard_tasks(struct repeating_timer *t)
{
        tuh_task();
        led_blinking_task();
        return true;
}

int app_lua(int, char**); // This is a Temporay 
#include "boot.h"
extern const uint16_t Pallets_256[][256];
extern const uint16_t Pallets[5][16];
int main()
{
    set_sys_clock_khz (250000, true);
    video_init();
    // stdio_init_all();

    // USB Init
    tusb_init();
    struct repeating_timer timer[2];
    add_repeating_timer_ms(50, Keyboard_tasks, NULL, &timer[0]);
    add_repeating_timer_ms(250, Blink_task, NULL, &timer[1]);

// This is the boot splash screen
    _Display_Mode = MODE_G3;                                    // Switch to 160x100 256 colour mode
    memcpy(_VBUFFERS.CLUT, boot_cmap, sizeof(boot_cmap));       // Load Boot screen colour lookup table
    memcpy(_VBUFFERS.VIDEO_BUFF, boot_data, sizeof(boot_data)); // Load Boot screen image
// Turn on overlay
    Overlay_clear(2);
    Overlay_draw_line(0,0,0,99,1);
    Overlay_draw_line(0,99,159,99,1);
    Overlay_draw_line(0,0,159,0,3);
    Overlay_draw_line(159,0,159,99,3);
  /*   Overlay_draw_line(0,0,159,99,13);
    Overlay_draw_circle(159,0,10,12);
    Overlay_print(0,0,7,"\x1b~0#\x1b~1#\x1b~2#\x1b~3#\x1b~4#\x1b~5#\x1b~6#\x1b~7#"
        "\x1b~8#\x1b~9#\x1b~A#\x1b~B#\x1b~C#\x1b~D#\x1b~E#\x1b~F#\x1b~G#\n\r"
        "\x1b#0FONT 0\n\r"
        "\x1b#1FONT 1\n\r"
        "\x1b#2FONT 2\n\r"
        "\x1b#3FONT 3\n\r"
        "\x1b#4FONT 4\n\r"
        );  */ 
    Overlay_print(2,2,0,"\x1b#2Lua\x1b#0 ver %s.%s.%s\n\r"
        "\x1b#2CPU\x1b#0 %s\n\r\x1b#2RAM\x1b#0 %d bytes\n\r\x1b#2PICO SDK\x1b#0 ver %s"
        ,LUA_VERSION_MAJOR,LUA_VERSION_MINOR,LUA_VERSION_RELEASE
        #if PICO_RP2350
        ,"RP2350 M33"
        #else
        ,"RP2040 M0+"
        #endif
        , HEAP_SIZE
        , PICO_SDK_VERSION_STRING
    );
    Overlay_draw_line(1,75,158,75,3);
    Overlay_draw_line(1,76,158,76,1);
    Overlay_fill_rect(1,77,157,22,0);
    Overlay_print(4,80,11,"\x1b#2\x80\x81  WAIT  \x84\x85  SCAN  \n\r\x82\x83        \x86\x87        ");
    // Overlay_print(0,80,11,"\x1b#2      Storage");
    //Overlay_print(0,74,14,"1234567890123456789012345678901234567890");
    _VBUFFERS.VIDEO_REG.OVERLAY_CLUT = 0;
    _VBUFFERS.VIDEO_REG.OVERLAY_ENABLE = 1;
    FATFS   vol;
    FRESULT fr = f_mount (&vol, "0:", 0);
    if ( fr == FR_OK )
    {  
        Overlay_print(60,80,0,"\x1b#2SCAN");
        f_mount (&vol, "0:", 1);
        Overlay_print(2,34,0,"\x1b#2Drive 0:\x1b#0 %.7s\n\r", &"NO DISK FAT12   FAT16   FAT32   EXFAT   "[vol.fs_type * 8]);
        if (vol.fs_type){
            Overlay_print(60,80,10,"\x1b#1OK");
        } else {
            Overlay_print(60,80,6, "\x1b#1NONE");
        }
        FRESULT fr;
        FILINFO fno;
    }
    //Overlay_print(0,90,12,"\x1b#2     Keyboard?");/**/
// Waits for keyboard
    while (!KeyBoard_Mounted) { 
        sleep_ms(1); 
        Overlay_print(4,80, _VBUFFERS.VIDEO_REG._Blink ? 11 : 0,"\x1b#2    WAIT");
    }
    Overlay_print(4,80, 0,"\x1b#2    WAIT");
    Overlay_print(4,80, 10,"\x1b#1   FOUND");
    sleep_ms(500); 
    _VBUFFERS.VIDEO_REG.OVERLAY_ENABLE = 0;
    Overlay_clear(0);

// Boot completed Switch to text mode 2
    _Display_Mode = MODE_T2;
    _VBUFFERS.VIDEO_REG.STAT_ENABLE = 1;
    memcpy(_VBUFFERS.CLUT,Pallets_256[0],sizeof(uint16_t) * 256);
    Reset_Character_Buffer();
    pt_reset_memory();
    printf("PT52 \ef01C\ef02O\ef03L\ef04O\ef05U\ef06L \ef09M\ef0aO\ef0bD\ef0cE\ef07\n\r");

    /* while (!stdio_usb_connected()) {
        sleep_ms(100);
    } */
    puts("Ready");
#ifdef SKIP_TESTLUA
    char *argvs[] = {"Lua"};
    app_lua(1,argvs);
#else
    ptsh_loop();
#endif

    printf("\r\n>> MAIN LOOP HAS ENDED PLEASE RESET! <<");
    do{sleep_ms(1);}while(1);    
}